public partial class PlanetData
{
    public void PopulateFrom(PlanetData otherPlanetData)
    {
        PlanetDataId = otherPlanetData.PlanetDataId;
        UpgradeCost = otherPlanetData.UpgradeCost;
        Health = otherPlanetData.Health;
        GoldMax = otherPlanetData.GoldMax;
        Scale = otherPlanetData.Scale;
        ShipRepairCost = otherPlanetData.ShipRepairCost;
    }
}