using System;

public partial class PlayerData
{
    public void PopulateFrom(PlayerData otherPlayerData)
    {
        PlayerDataId = Guid.NewGuid().ToString();
        ShipDataId = otherPlayerData.ShipDataId;
        WeaponDataId = otherPlayerData.WeaponDataId;
        ProjectileDataId = otherPlayerData.ProjectileDataId;
        PlanetDataId = otherPlayerData.PlanetDataId;
        ShieldDataId = otherPlayerData.ShieldDataId;
        SpeedDataId = otherPlayerData.SpeedDataId;
    }
}