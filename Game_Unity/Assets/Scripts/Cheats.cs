using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Toolset.Core;

public class Cheats : MonoBehaviour
{
    public void NewGame()
    {
        GameManager.Instance.NewGame("Mars");
    }

    public void MaxGold()
    {
        GameManager.Instance.HomePlanet.AddGold(GameManager.Instance.HomePlanet.PlanetData.GoldMax);
    }

    public void RemoveWater()
    {
        GameManager.Instance.HomePlanet.RemoveGold(1);
    }

    public void UpgradePlanet()
    {
        GameManager.Instance.UpgradePlanet();
    }

    public void SaveGame()
    {
        GameManager.Instance.ForceSave();
    }
    public void AttackSelf()
    {
        if (GameManager.Instance.IsPlayerDead)
        {
            Debug.LogWarning("Player is Dead can't attack");
            return;
        }

        GameManager.Instance.AttackSelf();
    }

    public void GetNeighbors()
    {
        GameManager.Instance.Network.GetNeighbors();
    }

    public void LoadData()
    {
        GameManager.Instance.Network.LoadData();
    }

    void Update()
    {
        switch(GameManager.Instance.State)
        {
            case GameManager.GameState.AtHomePlanet:
            case GameManager.GameState.PlayerRegen:
                // If Shift is pressed Invade
                if(Input.GetKeyUp(KeyCode.LeftShift))
                    GameManager.Instance.InvadeEnemyPlanet();
                break;
            case GameManager.GameState.InvadingEnemyPlanet:
            case GameManager.GameState.DestroyedEnemyPlanet:
                // If Shift is pressed Go Home
                if(Input.GetKeyUp(KeyCode.LeftShift))
                    GameManager.Instance.GoHome();
                break;
        }
    }
}
