using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public ShipData ShipData { get; private set; }
    protected string WeaponId { get; private set; }
    protected string ProjectileId { get; private set; }
    
    public virtual void PopulateFromStaticData(string shipDataId = StaticData.c_defaultId, string weaponDataId = StaticData.c_defaultId, string projectileDataId = StaticData.c_defaultId)
    {
        ShipData = StaticData.GetShipData(shipDataId);
        WeaponId = weaponDataId;
        ProjectileId = projectileDataId;
    }
}
