﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MainMenu : MonoBehaviour {
    public GameManager m_gameDirector;
    public GameObject m_player;
    public Image m_color1;
    public Image m_color2;
    public Image m_color3;
    public Image m_color4;
    public Image m_color5;
    public InputField m_inputName;

	// Use this for initialization
	void Start ()
    {
        GameManager.Instance.MainMenu = this;
        string name = PlayerPrefs.GetString("PlanetName").ToString();
        if (name == "")
            name = Environment.UserName;
        m_inputName.text = name;
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            ContinueGame();
        }
	}

    public void NewGame()
    {
        string planetName = m_inputName.text.Replace(" ", "_");
        if (planetName == "")
        {
            m_inputName.text = Environment.UserName;
            return;
        }

        PlayerPrefs.SetString("PlanetName", planetName);
        PlayerPrefs.Save();

        m_gameDirector.NewGame(planetName);
    }

    public void ContinueGame()
    {
        string planetName = m_inputName.text.Replace(" ", "_");
        if (planetName == "" || planetName == "Enter Planet Name")
        {
            m_inputName.text = Environment.UserName;
            return;
        }

        PlayerPrefs.SetString("PlanetName", planetName);
        PlayerPrefs.Save();
        m_gameDirector.ContinueGame(planetName);
    }

    private void UpdatePlayerColor(Color color)
    {
        GameObject playerShipObj = GameObject.FindGameObjectWithTag("Player");
        if (playerShipObj != null)
        { 
            PlayerShip playerShip = playerShipObj.GetComponent<PlayerShip>();
            playerShip.m_shipColor = color;
            playerShip.UpdateColor();
        }
    }

    public void ClickColor1()
    {
        UpdatePlayerColor(m_color1.color);
    }

    public void ClickColor2()
    {
        UpdatePlayerColor(m_color2.color);
    }

    public void ClickColor3()
    {
        UpdatePlayerColor(m_color3.color);
    }

    public void ClickColor4()
    {
        UpdatePlayerColor(m_color4.color);
    }

    public void ClickColor5()
    {
        UpdatePlayerColor(m_color5.color);
    }
}
