using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    public GameObject m_atHomeSection;
    public GameObject m_invadingSection;
    public GameObject m_enemyPlanetDestroyedSection;
    public GameObject m_playerDeadSection;
    public GameObject m_neighbors;

    public Text m_shipRegenCountdownInSec;
    public Text m_rewardText;

    private Button m_purchaseButtonComponent;
    private Text m_purchaseButtonText;
    private int m_shipPurchaseCost;
    private int m_originalRewardFontSize;

    public void Awake()
    {
        GameManager.Instance.GameUI = this;
        m_purchaseButtonText = m_playerDeadSection.GetComponentInChildren<Text>();
        m_purchaseButtonComponent = m_playerDeadSection.GetComponent<Button>();

        m_originalRewardFontSize = m_rewardText.fontSize;
    }

    public void Invade()
    {
        if (GameManager.Instance.IsPlayerDead)
        {
            Debug.LogWarning("Player is Dead can't invade");
            return;
        }

        GameManager.Instance.InvadeEnemyPlanet();
        m_rewardText.text = "";
    }

    public void GoHome()
    {
        GameManager.Instance.GoHome();
    }

    public void PurchaseShip()
    {
        if (GameManager.Instance.HomePlanet.Gold < m_shipPurchaseCost)
            return;

        var homePlanet = GameManager.Instance.HomePlanet;
        homePlanet.RemoveGold(m_shipPurchaseCost);

        GameManager.Instance.CreatePlayer();
        GameManager.Instance.DelayedSave();
    }

    public void UpdateShipRegenCountdown(int time)
    {
        m_shipRegenCountdownInSec.text = time.ToString();
    }

    private void OnEnable()
    {
        m_atHomeSection.SetActive(true);
        m_enemyPlanetDestroyedSection.SetActive(false);
        m_playerDeadSection.SetActive(false);
        EventManager.AddListener<GameStateChanged>(OnGameStateChanged);
        EventManager.AddListener<PlanetDataUpdated>(OnPlanetDataUpdated);
        EventManager.AddListener<EnemyDestroyed>(OnEnemyDestroyed);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<GameStateChanged>(OnGameStateChanged);
        EventManager.RemoveListener<PlanetDataUpdated>(OnPlanetDataUpdated);
        EventManager.RemoveListener<EnemyDestroyed>(OnEnemyDestroyed);
    }

    private void OnPlanetDataUpdated(PlanetDataUpdated evt)
    {
        m_shipPurchaseCost = (int)(evt.Data.ShipRepairCost);
        m_purchaseButtonText.text = $"PURCHASE SHIP {m_shipPurchaseCost.ToString()}";
    }

    private void OnEnemyDestroyed(EnemyDestroyed evt)
    {
        StopAllCoroutines();
        StartCoroutine(GoldRewardFeedbackRoutine(evt.Reward));
    }

    private IEnumerator GoldRewardFeedbackRoutine(int reward)
    {
        int currentRewardTally = GameManager.Instance.RewardTally - reward;

        while(reward > 0)
        {
            reward--;
            m_rewardText.text = currentRewardTally++.ToString();
            m_rewardText.fontSize = m_originalRewardFontSize + 6;
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            m_rewardText.fontSize = m_originalRewardFontSize;
        }
        m_rewardText.text = GameManager.Instance.RewardTally.ToString();
    }

    private void OnGameStateChanged(GameStateChanged evt)
    {
        switch(evt.State)
        {
            case GameManager.GameState.AtHomePlanet:
                m_atHomeSection.SetActive(true);
                m_invadingSection.SetActive(false);
                m_enemyPlanetDestroyedSection.SetActive(false);
                m_playerDeadSection.SetActive(false);
                m_neighbors.SetActive(true);
                break;
            case GameManager.GameState.PlayerRegen:
                m_atHomeSection.SetActive(false);
                m_invadingSection.SetActive(false);
                m_enemyPlanetDestroyedSection.SetActive(false);
                m_playerDeadSection.SetActive(true);
                m_neighbors.SetActive(true);
                break;
            case GameManager.GameState.InvadingEnemyPlanet:
                m_atHomeSection.SetActive(false);
                m_invadingSection.SetActive(true);
                m_enemyPlanetDestroyedSection.SetActive(false);
                m_playerDeadSection.SetActive(false);
                m_neighbors.SetActive(false);
                break;
            case GameManager.GameState.DestroyedEnemyPlanet:
                m_atHomeSection.SetActive(false);
                m_invadingSection.SetActive(true);
                m_enemyPlanetDestroyedSection.SetActive(true);
                m_playerDeadSection.SetActive(false);
                m_neighbors.SetActive(false);
                break;
        }
    }

    private void OnPlanetWaterUpdated(PlanetGoldUpdated evt)
    {
        if (GameManager.Instance.HomePlanet.Gold < m_shipPurchaseCost)
            m_purchaseButtonComponent.interactable = false;
        else
            m_purchaseButtonComponent.interactable = true;
    }
}
