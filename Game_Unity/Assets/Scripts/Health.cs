﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Health : MonoBehaviour
{
    public int maxHealth = 1;

    [SerializeField]
    private int m_currentHealth;
    public int CurrentHealth 
    { 
        get
        {
            return m_currentHealth;
        }
        set
        { 
            m_currentHealth = value; 

            if (fillBar != null)
            {
                fillBar.fillAmount = NormalizedHealth;
            }

            if (ChangedEvent != null)
            {
                ChangedEvent(this, new EventArgs());
            }
        }
    }
    public float NormalizedHealth => m_currentHealth / (float)maxHealth;
    
    public bool IsInvulnerable { get; set; }

    public bool IsDead { get => CurrentHealth <= 0; }

    public event EventHandler ChangedEvent;
    public event EventHandler DiedEvent;

    // Hit flashing
    public SpriteRenderer[] spriteRenderers;
    public Color hitFlashColor = Color.red;
    public Color originalColor;

    public Image fillBar;
    
    void Start()
    {
        if (spriteRenderers.Length > 0)
            originalColor = spriteRenderers[0].color;
    }

    private void OnEnable()
    {
        FullHeal();
    }

    public void SetMaxHealth(int newMaxHealth)
    {
        if (newMaxHealth <= 0)
        {
            return;
        }

        maxHealth = newMaxHealth;
        CurrentHealth = maxHealth;
    }

    public void Heal(int healAmount)
    {
        if (healAmount <= 0)
        {
            return;
        }

        CurrentHealth += healAmount;
        CurrentHealth = Mathf.Min(CurrentHealth, maxHealth);
    }

    public void FullHeal()
    {
        CurrentHealth = maxHealth;
    }

    private IEnumerator FlashUnit()
    {
        foreach(var sprite in spriteRenderers)
        {
            sprite.color = hitFlashColor;
            yield return new WaitForSeconds(0.3f);
            sprite.color = originalColor;
        }
    }

    public void ForceDeath()
    {
        Damage(maxHealth);
    }

    public void Damage(int damageAmount)
    {
        if (damageAmount <= 0)
        {
            return;
        }

        if (IsDead)
        {
            return;
        }

        if (IsInvulnerable)
        {
            return;
        }

        CurrentHealth = Mathf.Max(0, CurrentHealth - damageAmount);

        StartCoroutine(FlashUnit());

        // If dead, and there is a event handler, execute events.
        if (IsDead)
        {
            if (DiedEvent != null)
                DiedEvent(this, new EventArgs());

            foreach(var sprite in spriteRenderers)
                sprite.color = originalColor;
        }
    }
}
