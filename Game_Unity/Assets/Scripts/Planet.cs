﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour
{
    //
    // Planet
    //
    public PlanetData PlanetData;

    public Text m_planetNameText;
    public PlanetWater PlanetWater;
    public bool IsHomePlanet = false;

    public GameObject m_turretPrefab;
    public GameObject m_hangerPrefab;
    public GameObject m_fighterPrefab;
    public GameObject m_motherShipPrefab;
    public GameObject[] m_shieldPrefab;

    public int Gold => PlanetWater.Amount;

    public Health Health;
    protected int m_reward = 0;
    private CircleCollider2D m_circleCollider2D;

    public bool IsDead => Health.IsDead;

    public float CombatDistance => PlanetData.Scale * 2f;

    //
    // Visuals
    //
    public Transform m_sprite;
    private SpriteRenderer m_spriteRenderer;
    public float m_rotationSpeed = 1.0f;
    private Sprite m_normalSprite;
    public Sprite m_driedSprite;

    protected float m_scale;
    public float Scale
    {
        get { return m_scale; }
        set
        {
            m_scale = value;
            m_sprite.localScale = new Vector3(value, value, 1.0f);
            PlanetWater.PlanetScale = value;
        }
    }

    //
    // Sound
    //
    public AudioClip m_planetCrack;
    protected AudioSource m_audioSource;

    //
    // References to data
    //
    public TurretData TurretData { get; private set; }
    public HangerData HangerData { get; private set; }
    public FighterData FighterData { get; private set; }
    public PlanetShieldData PlanetShieldData { get; private set; }
    public FleetData FleetData { get; private set; }


    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();
        m_circleCollider2D = GetComponent<CircleCollider2D>();

        m_spriteRenderer = m_sprite.GetComponentInChildren<SpriteRenderer>();
        m_normalSprite = m_spriteRenderer.sprite;

        Health = GetComponent<Health>();
        Health.DiedEvent += OnDeath;
    }

    virtual protected void OnDeath(object sender, System.EventArgs e)
    {
        // Show planet death only if not home planet
        if (IsHomePlanet)
            return;

        // Also despawn ground defenses
        foreach(Unit unit in GetComponentsInChildren<Unit>())
        {
            unit.ForceDeath();
        }

        m_spriteRenderer.sprite = m_driedSprite;
        if(!m_audioSource.isPlaying)
            m_audioSource.PlayOneShot(m_planetCrack, 3f);

        UnitRewardFeedback feedback = GetComponentInChildren<UnitRewardFeedback>(includeInactive: true);
        feedback.ShowFeedback(m_reward);

        EventManager.Fire(new EnemyDestroyed(m_reward));
    }

    void Update () 
    {
        if (GameManager.Instance.State == GameManager.GameState.MainMenu)
        {
            transform.rotation = Quaternion.identity;
        }
        else
        {
            // Planet rotation
            transform.Rotate(0, 0, m_rotationSpeed * Time.deltaTime);
            if (m_planetNameText != null)
                m_planetNameText.transform.up = Vector3.up;
        }
	}

    public void SetData(PlanetData planetData = null)
    {
        if (planetData != null)
            PlanetData = planetData;

        TearDownPlanet();

        PlanetWater.SetFullAmmount(PlanetData.xGoldCount, PlanetData.GoldMax);
        Scale = PlanetData.Scale;

        if (m_circleCollider2D != null)
            m_circleCollider2D.radius = Scale * 0.5f;

        m_spriteRenderer.sprite = m_normalSprite;

        if (Health == null)
            Health = GetComponent<Health>();
        Health?.SetMaxHealth(PlanetData.Health);


        m_reward = (int)(PlanetData.UpgradeCost * GameManager.Instance.m_attackRewardScalar);
        if (!string.IsNullOrEmpty(PlanetData.xTurretId))
        {
            TurretData = StaticData.GetTurretData(PlanetData.xTurretId);
            EventManager.Fire(new TurretDataUpdated(TurretData));
        }
        if (!string.IsNullOrEmpty(PlanetData.xHangerId))
        {
            HangerData = StaticData.GetHangerData(PlanetData.xHangerId);
            EventManager.Fire(new HangerDataUpdated(HangerData));
        }
        if (!string.IsNullOrEmpty(PlanetData.xFighterId))
        {
            FighterData = StaticData.GetFighterData(PlanetData.xFighterId);
            EventManager.Fire(new FighterDataUpdated(FighterData));
        }
        if (!string.IsNullOrEmpty(PlanetData.xShieldId))
        {
            PlanetShieldData = StaticData.GetPlanetShieldData(PlanetData.xShieldId);
            EventManager.Fire(new PlanetShieldDataUpdated(PlanetShieldData));
        }
        if (!string.IsNullOrEmpty(PlanetData.xFleetId))
        {
            FleetData = StaticData.GetFleetData(PlanetData.xFleetId);
            EventManager.Fire(new FleetDataUpdated(FleetData));
        }

        if (TurretData != null)
        {
            for (int i = 0; i < PlanetData.xTurretCount; i++)
            {
                AddTurret(TurretData, true);
            }
        }

        if (HangerData != null)
        {
            for (int i = 0; i < PlanetData.xHangerCount; i++)
            {
                AddHanger(HangerData, true);
            }
        }

        if (FighterData != null)
        {
            for (int i = 0; i < PlanetData.xFighterCount; i++)
            {
                AddFighter(FighterData, true);
            }
        }

        if (PlanetShieldData != null)
        {
            if (PlanetData.xShieldCount > 0)
                AddShield(PlanetShieldData, true);
        }

        if (FleetData != null)
        {
            for (int i = 0; i < PlanetData.xFleetCount; i++)
            {
                AddFleet(FleetData, true);
            }
        }

        if (m_planetNameText != null)
            m_planetNameText.text = planetData.xPlanetName;
    }

    public void TearDownPlanet()
    {
        foreach(var defenseUnit in GameObject.FindObjectsOfType<Unit>())
        {
            ObjectPoolManager.Instance.Despawn(defenseUnit.gameObject);
        }

        foreach(var projectile in GameObject.FindObjectsOfType<Projectile>())
        {
            ObjectPoolManager.Instance.Despawn(projectile.gameObject);
        }

        if (m_planetNameText != null)
            m_planetNameText.text = "";
    }

    public void AddGold(int amount)
    {
        PlanetWater.Add(amount);
    }

    public void RemoveGold(int amount)
    {
        PlanetWater.Remove(amount);
    }

    public GameObject AddPlanetGroundDefense(GameObject prefab)
    {
        var turretPos = transform.position + new Vector3(0.0f, m_scale * 0.5f, 1.0f);
        Quaternion turretRot = Quaternion.Euler(0.0f, 0.0f, Random.Range(0, 360));
        turretPos = turretRot * turretPos;

        GameObject unitObj = ObjectPoolManager.Instance.Spawn(prefab, turretPos, turretRot, transform) as GameObject;

        var health = unitObj.GetComponent<Health>();
        if (health != null)
            health.FullHeal();

        return unitObj;
    }

    public Turret AddTurret(TurretData turretData, bool isInit = false)
    {
        var go = AddPlanetGroundDefense(m_turretPrefab);
        Turret turret = go.GetComponent<Turret>();
        turret.SetData(turretData);
        if (!isInit)
            PlanetData.xTurretCount++;
        return turret;
    }    

    public Hanger AddHanger(HangerData hangerData, bool isInit = false)
    {
        var go = AddPlanetGroundDefense(m_hangerPrefab);
        Hanger hanger = go.GetComponent<Hanger>();
        hanger.SetData(hangerData);
        if (!isInit)
            PlanetData.xHangerCount++;
        return hanger;
    }    

    public Fighter AddFighter(FighterData fighterData, bool isInit = false)
    {
        var shipPos = transform.position + new Vector3(0.0f, m_scale * 0.5f + 1.5f, 1.0f);
        Quaternion shipRot = Quaternion.Euler(0.0f, 0.0f, Random.Range(0, 360));
        shipPos = shipRot * shipPos;

        GameObject unitObj = ObjectPoolManager.Instance.Spawn(m_fighterPrefab, shipPos, shipRot) as GameObject;

        Fighter fighter = unitObj.GetComponent<Fighter>();
        fighter.SetData(fighterData);

        if (!isInit)
            PlanetData.xFighterCount++;
        return fighter;
    }

    public PlanetShield AddShield(PlanetShieldData shieldData, bool isInit = false)
    {
        // Delete existing shield and add a new one
        var shield = GetComponentInChildren<PlanetShield>();
        if (shield != null)
            ObjectPoolManager.Instance.Despawn(shield.gameObject);

        // TODO - add shield
        // TODO : Set data
        var shieldPos = transform.position;

        GameObject shieldObj = ObjectPoolManager.Instance.Spawn(m_shieldPrefab[PlanetData.xShieldLevel], shieldPos, Quaternion.Euler(0.0f, 0.0f, Random.Range(0, 360)), transform) as GameObject;
        if (!isInit)
            PlanetData.xShieldCount = 1;

        float shieldSize = 7 + Scale;
        shieldObj.transform.localScale = new Vector3(shieldSize, shieldSize, shieldSize);

        PlanetShield planetShield = shieldObj.GetComponent<PlanetShield>();
        planetShield.SetData(shieldData);

        return planetShield;
    }

    public Mothership AddFleet(FleetData fleetData, bool isInit = false)
    {
        var shipPos = transform.position + new Vector3(0.0f, m_scale * 0.5f + 3f, 1.0f);
        Quaternion shipRot = Quaternion.Euler(0.0f, 0.0f, Random.Range(0, 360));
        shipPos = shipRot * shipPos;

        GameObject unitObj = ObjectPoolManager.Instance.Spawn(m_motherShipPrefab, shipPos, shipRot) as GameObject;

        Mothership motherShip = unitObj.GetComponent<Mothership>();
        motherShip.SetData(fleetData);

        if (!isInit)
            PlanetData.xFleetCount++;
        return motherShip;
    }

    public void SetTurretData(TurretData data)
    {
        TurretData = data;
        EventManager.Fire(new TurretDataUpdated(data));
    }

    public void SetHangerData(HangerData data)
    {
        HangerData = data;
        EventManager.Fire(new HangerDataUpdated(data));
    }

    public void SetFighterData(FighterData data)
    {
        FighterData = data;
        EventManager.Fire(new FighterDataUpdated(data));
    }

    public void SetPlanetShieldData(PlanetShieldData data)
    {
        PlanetShieldData = data;

        AddShield(data);

        EventManager.Fire(new PlanetShieldDataUpdated(data));
    }

    public void SetFleetData(FleetData data)
    {
        FleetData = data;
        EventManager.Fire(new FleetDataUpdated(data));
    }
}
