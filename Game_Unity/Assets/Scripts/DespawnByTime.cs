﻿using UnityEngine;
using System.Collections;

public class DespawnByTime : MonoBehaviour
{
    public float m_despawnByTime = 2f;

	// Use this for initialization
	void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(DespawnRoutine());
	}

    IEnumerator DespawnRoutine()
    {
        yield return new WaitForSeconds(m_despawnByTime);
		ObjectPoolManager.Instance.Despawn(gameObject);
    }
}
