using System;
using Toolset.Core;
using Toolset.Networking;

public class DownloadPlanetDataRequest : HttpRequest<string, PlanetData>
{
    public DownloadPlanetDataRequest(string playerPlanetDataId) : base(playerPlanetDataId)
    {
        NetworkRequestSettings.RetryPolicy = RequestRetryPolicy.None;
    }

    protected override HttpRequestMethod HttpRequestMethod => HttpRequestMethod.Put;

    protected override Uri Url => new Uri(ToolsetConstants.c_remoteUrl.StringBuilderAppend("DownloadPlanetData"));
    
    
}
