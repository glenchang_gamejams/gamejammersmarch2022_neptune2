﻿using UnityEngine;
using System.Collections;

public class OnDeathDestroy : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        // Handler for death
        Health health = GetComponent<Health>();
        if (health != null)
            health.DiedEvent += OnDeath;
    }

    public void OnDeath(object sender, System.EventArgs e)
    {
        ObjectPoolManager.Instance.Despawn(gameObject);
    }
}
