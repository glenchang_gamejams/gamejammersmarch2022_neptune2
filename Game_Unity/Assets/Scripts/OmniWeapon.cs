﻿using UnityEngine;
using System.Collections;

public class OmniWeapon : Weapon 
{
    override public void FireProjectile(Transform target)
    {
        // This should later be converted to a function to recycle instances
        GameObject projectileObj = ObjectPoolManager.Instance.Spawn(projectilePrefab, transform.position + -transform.up, transform.rotation) as GameObject;
        Projectile projectile = projectileObj.GetComponent<Projectile>();
        if (projectile != null)
        {

            // Place the projectile into the correct layer
            if (hitLayer == hitLayerEnum.CanHitEnemy)
                projectileObj.layer = Layers.CanHitEnemy;
            else if (hitLayer == hitLayerEnum.CanHitPlayer)
                projectileObj.layer = Layers.CanHitPlayer;
            // else it hits everything

            var dir = (target.position - transform.position).normalized;
            projectile.SetVelocity(dir * WeaponData.ProjectileSpeed);
            projectile.transform.rotation = Quaternion.FromToRotation(Vector3.up, dir);
        }
        var health = projectileObj.GetComponent<Health>();
        if (health != null)
            health.FullHeal();
    }
}
