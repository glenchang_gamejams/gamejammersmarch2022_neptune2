using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GoldGenerator : MonoBehaviour
{
    private const string c_getServerTimeUrl = "https://www.glenchang.com/main/games/Neptune_II/GetServerTime.php?";
    private float m_goldRegenRateInSec;

    private void Start()
    {
        GameManager.Instance.GoldGenerator = this;
        m_goldRegenRateInSec = GameManager.Instance.m_goldRegenerationRateBySec;
    }

    public void StartGoldGenerator()
    {
        StartCoroutine(AwardOfflineGoldRoutine());
        StartCoroutine(GenerateRuntimeGoldRoutine());
    }

    private IEnumerator GenerateRuntimeGoldRoutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(m_goldRegenRateInSec);
            GameManager.Instance.HomePlanet.AddGold(1);
        }
    }

    private IEnumerator AwardOfflineGoldRoutine()
    {
        // Get time from server
        using (UnityWebRequest webRequest = UnityWebRequest.Get($"{c_getServerTimeUrl}noCacheRandom={UnityEngine.Random.Range(0f, float.MaxValue)}"))
        {
            webRequest.useHttpContinue = false;
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Web request error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("Web request HTTP error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    string result = webRequest.downloadHandler.text;

                    // Get last timestamp
                    int lastTimeStamp = GameManager.Instance.PlayerData.xTimeStamp;
                    int currentTimeStamp = Int32.Parse(result);

                    // If the last timestamp is 0, then player has never timestamped before, ignore this instance
                    if (lastTimeStamp != 0)
                    {
                        float reward = ((currentTimeStamp - lastTimeStamp) / m_goldRegenRateInSec);
                        Debug.Log($"Player was away for {currentTimeStamp - lastTimeStamp} sec award is {reward} ");
                        GameManager.Instance.HomePlanet.AddGold((int)reward);
                    }
                    GameManager.Instance.PlayerData.xTimeStamp = currentTimeStamp;
                    break;
            }
        }
    }
}
