using System.Collections.Generic;
using UnityEngine;

    public class ConsoleLog : MonoBehaviour
    {
        [SerializeField]
        private bool m_isEnabled = true;

        [SerializeField]
        private int m_maxLogLines = 5;
        
        //#if !UNITY_EDITOR
        static string myLog = "";
        private string output;
        private string stack;
        
        private Queue<string> m_messages = new Queue<string>();

        void OnEnable()
        {
            Application.logMessageReceived += Log;
        }
     
        void OnDisable()
        {
            Application.logMessageReceived -= Log;
        }
     
        public void Log(string logString, string stackTrace, LogType type)
        {
            if (!m_isEnabled)
                return;
            
            m_messages.Enqueue(logString + "\n");

            if (m_messages.Count > m_maxLogLines)
                m_messages.Dequeue();
            
            myLog = string.Empty;

            foreach (var item in m_messages)
            {
                myLog += item;
            }
        }
     
        void OnGUI()
        {
            if (m_isEnabled) //Do not display in editor ( or you can use the UNITY_EDITOR macro to also disable the rest)
            {
                myLog = GUI.TextArea(new Rect(10, 10, Screen.width * 0.25f, Screen.height * 0.25f), myLog);
            }
        }
        //#endif
    }
