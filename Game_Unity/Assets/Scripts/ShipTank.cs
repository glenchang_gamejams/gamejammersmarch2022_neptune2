﻿using UnityEngine;
using System.Collections;

public class ShipTank : MonoBehaviour
{
    public Sprite openSprite;
    private Sprite closeSprite;
    private SpriteRenderer m_renderer;

    public float maxWater = 500.0f;
    public float waterDrainRate = 100.0f;
    public float waterDrainRange = 5.0f;

    void Start()
    {
        m_renderer = GetComponent<SpriteRenderer>();
        closeSprite = m_renderer.sprite;
    }

    public void OpenTank()
    {
        if (m_renderer != null)
            m_renderer.sprite = openSprite;
    }

    public void CloseTank()
    {
        if (m_renderer != null)
            m_renderer.sprite = closeSprite;
    }
}
