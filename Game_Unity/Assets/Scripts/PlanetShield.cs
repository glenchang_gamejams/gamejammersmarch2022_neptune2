﻿using UnityEngine;
using System;

public class PlanetShield : Unit
{
    private PlanetShieldData m_planetShieldData;
    public float m_rechargeRate;
    private float m_rotationSpeed = 22f;
    private PolygonCollider2D m_collider;

    private float m_nextRechargeTime;

    private float m_rebootCooldownTime = 0f;

    public void SetData(PlanetShieldData planetShieldData)
    {
        m_planetShieldData = planetShieldData;

        m_timePassed = 0.0f;
        m_rechargeRate = planetShieldData.RechargeRate;

        SetHealth(planetShieldData.Health);

        m_nextRechargeTime = Time.time + m_rechargeRate;
        m_rotationSpeed = m_planetShieldData.Health * 2f;

        m_nextRechargeTime = m_timePassed + m_rechargeRate;
        m_collider.enabled = true;
    }

    // Use this for initialization
    void Awake()
    {
        Init();

        Health = GetComponent<Health>();
        Health?.FullHeal();

        m_collider = GetComponent<PolygonCollider2D>();

        Health.DiedEvent += OnDeath; ;
        Health.ChangedEvent += OnHealthChanged;
    }

    // Update is called once per frame
    void Update()
    {
        // Enemy behaviour
        if (!IsEnemy)
            return;

        m_timePassed += Time.deltaTime;
        if (m_timePassed < m_startupTime)
        {
            if (m_sprites != null)
            {
                foreach (var sprite in m_sprites)
                {
                    var color = sprite.color;
                    color.a = m_timePassed / m_startupTime;
                    sprite.color = color;
                }
            }
            return;
        }

        // If shield is down, wait for cooldown to reboot
        if (m_timePassed < m_rebootCooldownTime)
            return;

        // Recharge
        //
        if (m_timePassed > m_nextRechargeTime)
        {
            m_nextRechargeTime = m_timePassed + m_rechargeRate;

            Health.Heal(1);
        }

        // Rotation
        //
        float angle = Mathf.Atan2(m_player.position.y - transform.position.y, m_player.position.x - transform.position.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, 0, angle-90);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, m_rotationSpeed * Time.deltaTime);
    }

    override public void ForceDeath()
    {
        base.ForceDeath();
        ObjectPoolManager.Instance.Despawn(gameObject);
    }

    private float TransformAngle(float angle)
    {
        while (angle < 0.0f)
        {
            angle += 360.0f;
        }
        while (angle > 360.0f)
        {
            angle -= 360.0f;
        }

        return angle;
    }

    override protected void OnDeath(object sender, System.EventArgs e)
    {
        if (!IsEnemy)
            return;

        m_rebootCooldownTime = m_timePassed + 2f;
    }

    // Dim the sprite to show damage
    protected void OnHealthChanged(object sender, System.EventArgs e)
    {
        if (!IsEnemy)
            return;

        if (m_sprites != null)
        {
            foreach (var sprite in m_sprites)
            {
                var color = sprite.color;
                color.a = Health.NormalizedHealth;
                sprite.color = color;
            }
        }

        m_collider.enabled = !Health.IsDead;
    }
}
