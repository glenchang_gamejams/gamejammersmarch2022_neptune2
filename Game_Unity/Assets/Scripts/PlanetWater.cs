﻿using UnityEngine;
using Toolset.Core;
using System;

public class PlanetWater : MonoBehaviour
{
    // Visuals
    public ParticleSystem m_waterParticles;
    protected ParticleSystem.MainModule m_waterParticlesMain;
    private float m_planetScale;

    // Water
    [SerializeField]
    private int m_maxWater = 1;
    public int MaxWater
    {
        get => m_maxWater;
        set
        {
            if (value <= 0)
                return;

            m_maxWater = value;
            WaterUpdated();
        }
    }

    [SerializeField]
    private int m_amount;
    public int Amount 
    { 
        get => m_amount;
        set
        {
            m_amount = value;
            WaterUpdated();
        }
    }

    public void SetFullAmmount(int amount, int max)
    {
        m_maxWater = max;
        m_amount = amount;
        WaterUpdated();
    }
    
    public bool IsDepleted { get => Amount <= 0; }

    public event EventHandler UpdatedHandler;
    public event EventHandler DepletedHandler;

    public float PlanetScale
    {
        get { return m_planetScale; }
        set
        {
            m_planetScale = value;
            var shape = m_waterParticles.shape;
            shape.radius = value * 0.5f;
            WaterUpdated();
        }
    }

    public void WaterUpdated()
    {
        if (m_waterParticles != null)
        {
            var particleMain = m_waterParticles.main;
            particleMain.startSize = m_planetScale / 5f * (m_amount / (float)m_maxWater);
        }

        if (UpdatedHandler != null)
            UpdatedHandler(this, new EventArgs());

        EventManager.Fire(new PlanetGoldUpdated(Amount, MaxWater));
    }

    private void OnEnable()
    {
        WaterUpdated();
    }

    private void Awake()
    {
        m_waterParticlesMain = m_waterParticles.main;
    }

    public void Add(int amount)
    {
        if (amount <= 0)
        {
            Debug.Log($"Removing an invalid amount {amount}");
            return;
        }

        m_amount += amount;
        Amount = Mathf.Min(m_amount, m_maxWater);
    }

    public void Remove(int amount)
    {
        if (amount <= 0)
        {
            Debug.Log($"Removing an invalid amount {amount}");
            return;
        }

        if (!IsDepleted)
        {
            Amount = Mathf.Max(0, m_amount - amount);

        }

        if (IsDepleted && DepletedHandler != null)
        {
            DepletedHandler(this, new EventArgs());
        }
    }
}
