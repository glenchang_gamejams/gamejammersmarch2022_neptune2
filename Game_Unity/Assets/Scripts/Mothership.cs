﻿using UnityEngine;
using System.Collections;

public class Mothership : Fighter
{
    public GameObject m_weaponPrefab;
    public GameObject m_turretPrefab;
    public GameObject m_hangerPrefab;

    public FleetData m_fleetData;

    public void SetData(FleetData fleetData)
    {
        m_fleetData = fleetData;
        m_startupTime = fleetData.StartupTime;

        m_timePassed = 0.0f;
        Reward = (int)(fleetData.Cost * GameManager.Instance.m_attackRewardScalar);

        SetHealth(fleetData.Health);
    }

    // Use this for initialization
    override public void Init ()
    {
        base.Init();
        // Generate all turrets
        foreach(Transform hp in transform)
        {
            // Only spawn turrets where there are none
            if (hp.childCount > 0)
                foreach (Transform child in hp)
                    ObjectPoolManager.Instance.Despawn(child.gameObject);

            // Only spawn weapons for hardpoints HP_Weapon
            if (hp.name.StartsWith("HP_Weapon") && m_weaponPrefab != null)
            {
                GameObject turretObj = ObjectPoolManager.Instance.Spawn(m_weaponPrefab, hp) as GameObject;
                Turret turret = turretObj.GetComponent<Turret>();
                turret.SetData(GameManager.Instance.State == GameManager.GameState.AtHomePlanet ?
                    GameManager.Instance.HomePlanet.TurretData :
                    GameManager.Instance.EnemyPlanet.TurretData);
                turret.IsEnemy = IsEnemy;
            }
            // Only spawn turrets for hardpoints HP_Turret
            if (hp.name.StartsWith("HP_Turret") && m_turretPrefab != null)
            {
                GameObject turretObj = ObjectPoolManager.Instance.Spawn(m_turretPrefab, hp) as GameObject;
                Turret turret = turretObj.GetComponent<Turret>();
                turret.SetData(GameManager.Instance.State == GameManager.GameState.AtHomePlanet ?
                    GameManager.Instance.HomePlanet.TurretData :
                    GameManager.Instance.EnemyPlanet.TurretData);
                turret.IsEnemy = IsEnemy;
            }
            // Only spawn hangers for hardpoints HP_Hanger
            if (hp.name.StartsWith("HP_Hanger") && m_hangerPrefab != null)
            {
                GameObject hangerObj = ObjectPoolManager.Instance.Spawn(m_hangerPrefab, hp) as GameObject;
                Hanger hanger = hangerObj.GetComponent<Hanger>();
                hanger.SetData(GameManager.Instance.State == GameManager.GameState.AtHomePlanet ?
                    GameManager.Instance.HomePlanet.HangerData :
                    GameManager.Instance.EnemyPlanet.HangerData);
                hanger.IsEnemy = IsEnemy;
            }
        }
    }
}
