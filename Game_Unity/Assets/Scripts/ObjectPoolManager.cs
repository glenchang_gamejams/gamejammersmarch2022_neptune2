﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : SingletonBehaviour<ObjectPoolManager>
{
    public Dictionary<string, Stack<GameObject>> m_pool = new Dictionary<string, Stack<GameObject>>();

    public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        if (parent == null)
            parent = transform;

        if (!m_pool.ContainsKey(prefab.name))
        {
            m_pool.Add(prefab.name, new Stack<GameObject>());
        }

        var pool = m_pool[prefab.name];
        if (pool.Count == 0)
        {
            var newObj = Instantiate(prefab, position, rotation, parent) as GameObject;
            newObj.name = prefab.name;
            return newObj;
        }

        var oldObj = pool.Pop();
        if (oldObj == null)
        {
            var newObj = Instantiate(prefab, position, rotation, parent) as GameObject;
            newObj.name = prefab.name;
            return newObj;
        }
        oldObj.transform.SetParent(parent == null ? transform : parent);
        oldObj.transform.position = position;
        oldObj.transform.rotation = rotation;
        oldObj.SetActive(true);
        return oldObj;
    }

    public GameObject Spawn(GameObject prefab, Transform parent = null)
    {
        return Spawn(prefab, parent.position, parent.rotation, parent);
    }

    public void Cleanup()
    {
        foreach (var pool in m_pool)
        {
            if (pool.Value.Count < 10)
                continue;

            int shrink = pool.Value.Count * 3 / 4;
            for (int i = 0; i < shrink; ++i)
            {
                Destroy(pool.Value.Pop());
            }
        }
    }

    public void Despawn(GameObject obj)
    {
        if (obj == null)
            return;

        if (!m_pool.ContainsKey(obj.name))
        {
            Destroy(obj);
            return;
        }

        obj.transform.SetParent(transform);
        obj.SetActive(false);
        m_pool[obj.name].Push(obj);
    }

    public void Despawn(GameObject obj, float time)
    {
        DespawnByTime despawnTimer = obj.GetComponent<DespawnByTime>();
        if (despawnTimer == null)
        {
            // Add a Despawn Timer if it doesn't exist
            despawnTimer = obj.AddComponent<DespawnByTime>();
        }
        despawnTimer.enabled = true;
        despawnTimer.m_despawnByTime = time;
    }

    public override void OnAwake()
    {
        GameManager.Instance.ObjectPoolManager = this;
    }
}
