﻿using UnityEngine;
using System.Collections;

public class OnDeathExplode : MonoBehaviour
{
    public GameObject explosionPrefab;

    // Use this for initialization
    void Start ()
    {
        // Handler for death
        Health shipHealth = GetComponent<Health>();
        if(shipHealth!= null)
            shipHealth.DiedEvent += OnDeath;
    }
	
    public void OnDeath(object sender, System.EventArgs e)
    {
        if (explosionPrefab != null)
        {
            GameObject explosionObj = ObjectPoolManager.Instance.Spawn(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosionObj.transform.localScale = transform.localScale;

            Unit unit = GetComponent<Unit>();
            if (unit != null)
            {
                UnitRewardFeedback feedback = explosionObj.GetComponentInChildren<UnitRewardFeedback>(includeInactive:true);
                feedback.ShowFeedback(unit.Reward);
            }
        }

        ObjectPoolManager.Instance.Despawn(gameObject);
    }
}
