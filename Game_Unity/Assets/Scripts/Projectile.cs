﻿using System;
using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    public ProjectileData ProjectileData { get; protected set; }
    private Rigidbody2D rigidBody;

    // Use this for initialization
    void Awake ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Reset anything that needs to be reset from object pool respawning
    private void OnEnable()
    {
        rigidBody.velocity = Vector3.zero;
    }

    public void SetStaticData(ProjectileData dataFromWeapon)
    {
        ProjectileData = dataFromWeapon;
        ObjectPoolManager.Instance.Despawn(gameObject, ProjectileData.LifeSpan);
    }

    // Use this to set the velocity of the projectile
    public void SetVelocity(Vector2 velocity)
    {
        if (rigidBody == null)
        {
            return;
        }
        rigidBody.velocity = new Vector2(velocity.x, velocity.y);
    }
    
    //void OnBecameInvisible()
    //{
    //    // This should later be converted to a function to recycle instances
    //    ObjectPool.Instance.Despawn(gameObject);
    //}
    
    void OnTriggerEnter2D(Collider2D other)
    {
        Health otherHealth = other.gameObject.GetComponent<Health>();
        if (otherHealth != null)
        {
            otherHealth.Damage((int) ProjectileData.DamageAmount);
        }

        if (ProjectileData.DestroyOnCollide)
        {
            ObjectPoolManager.Instance.Despawn(gameObject);
        }
    }
}
