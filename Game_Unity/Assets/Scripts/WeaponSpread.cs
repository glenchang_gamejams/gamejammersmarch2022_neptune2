﻿using UnityEngine;
using System.Collections;

public class WeaponSpread : Weapon
{
    override public void FireProjectile(Transform target)
    {
        // This should later be converted to a function to recycle instances
        GameObject projectileObj = ObjectPoolManager.Instance.Spawn(projectilePrefab, transform.position + -transform.up, transform.rotation) as GameObject;
        Projectile projectile = projectileObj.GetComponent<Projectile>();
        if (projectile != null)
        {

            // Place the projectile into the correct layer
            if (hitLayer == hitLayerEnum.CanHitEnemy)
                projectileObj.layer = Layers.CanHitEnemy;
            else if (hitLayer == hitLayerEnum.CanHitPlayer)
                projectileObj.layer = Layers.CanHitPlayer;
            // else it hits everything

            projectile.SetVelocity(-transform.up * WeaponData.ProjectileSpeed * 0.5f);
            projectile.transform.rotation = Quaternion.FromToRotation(Vector3.up, -transform.up);
        }

        // This should later be converted to a function to recycle instances
        GameObject projectileObj2 = ObjectPoolManager.Instance.Spawn(projectilePrefab, transform.position + -transform.up, transform.rotation) as GameObject;
        Projectile projectile2 = projectileObj2.GetComponent<Projectile>();
        if (projectile2 != null)
        {

            // Place the projectile into the correct layer
            if (hitLayer == hitLayerEnum.CanHitEnemy)
                projectileObj2.layer = Layers.CanHitEnemy;
            else if (hitLayer == hitLayerEnum.CanHitPlayer)
                projectileObj2.layer = Layers.CanHitPlayer;
            // else it hits everything

            projectile2.SetVelocity((-transform.up +transform.forward) * WeaponData.ProjectileSpeed *0.5f);
            projectile2.transform.rotation = Quaternion.FromToRotation(Vector3.up, -transform.up);
        }

        // This should later be converted to a function to recycle instances
        GameObject projectileObj3 = ObjectPoolManager.Instance.Spawn(projectilePrefab, transform.position + -transform.up, transform.rotation) as GameObject;
        Projectile projectile3 = projectileObj3.GetComponent<Projectile>();
        if (projectile3 != null)
        {

            // Place the projectile into the correct layer
            if (hitLayer == hitLayerEnum.CanHitEnemy)
                projectileObj3.layer = Layers.CanHitEnemy;
            else if (hitLayer == hitLayerEnum.CanHitPlayer)
                projectileObj3.layer = Layers.CanHitPlayer;
            // else it hits everything

            projectile3.SetVelocity((-transform.up -transform.forward) * WeaponData.ProjectileSpeed * 0.5f);
            projectile3.transform.rotation = Quaternion.FromToRotation(Vector3.up, -transform.up);
        }
    }
}
