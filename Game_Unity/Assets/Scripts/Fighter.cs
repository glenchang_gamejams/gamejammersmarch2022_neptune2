﻿using UnityEngine;

public class Fighter : Unit {
    public float m_force = 3.0f;
    public float m_Avoidforce = 5.0f;
    public float m_maxForce = 5.0f;
    public float m_maxSpeed = 5.0f;
    public float m_fireArc = 30.0f;
    public float m_lookAhead = 12.0f;
    public Weapon m_weapon;
    private Rigidbody2D m_rigidBody;
    int m_avoidanceLayers = 0;

    private FighterData m_fighterData;

    public void SetData(FighterData fighterData)
    {
        m_fighterData = fighterData;

        // To offset other unit fire times
        m_timePassed = Random.Range(0.0f, 3f);

        Reward = (int)(fighterData.Cost * GameManager.Instance.m_attackRewardScalar);

        SetHealth(fighterData.Health);
    }

    // Use this for initialization
    void Start () {
        Init();

        Health = GetComponent<Health>();
        Health?.FullHeal();

        Health.DiedEvent += OnDeath;

        m_rigidBody = GetComponent<Rigidbody2D>();

        m_avoidanceLayers = 1 << Layers.Planet;
        m_avoidanceLayers |= 1 << Layers.Enemy;
        m_avoidanceLayers |= 1 << Layers.Player;

        if (m_weapon != null)
            m_weapon.PopulateFromStaticData();
	}

    void Update()
    {
        // Enemy behaviour
        if (!IsEnemy)
            return;

        m_timePassed += Time.deltaTime;
        if (m_timePassed < m_startupTime)
        {
            if (m_sprites != null)
            {
                foreach (var sprite in m_sprites)
                {
                    var color = sprite.color;
                    color.a = m_timePassed / m_startupTime;
                    sprite.color = color;
                }
            }
            return;
        }

        // Handle steering
        var steering = Seek();
        steering += Avoid();
        if (steering.magnitude > m_maxForce)
        {
            steering = steering.normalized * m_maxForce;
        }
        m_rigidBody.AddForce(steering);
        transform.rotation = Quaternion.FromToRotation(Vector3.up, m_rigidBody.velocity.normalized);

        // Fire weapon if enemy is in front
        var dir = (m_player.transform.position - transform.position).normalized;
        var targetDir = Quaternion.FromToRotation(Vector3.up, dir);

        m_weapon?.Fire(m_player.transform);

        //if (Mathf.Abs(Quaternion.Angle(transform.rotation, targetDir)) < m_fireArc)
        //{
        //    m_weapon?.Fire(m_player.transform);
        //}
	}

    Vector2 Seek () {
        var desired_velocity = (m_player.position - transform.position).normalized * m_maxSpeed;
        return new Vector2(desired_velocity.x, desired_velocity.y) - m_rigidBody.velocity;
    }

    Vector2 Avoid ()
    {
        var hit = Physics2D.Raycast(transform.position + (transform.up * 2.0f), transform.up, m_lookAhead, m_avoidanceLayers);
        if (hit.collider != null)
        {
            //var avoidPos = hit.collider.transform.position;
            //var hitAvoidance = hit.point - new Vector2(avoidPos.x, avoidPos.y);
            var dist = (new Vector2(transform.position.x, transform.position.y) - hit.point).magnitude;

            return hit.normal * m_Avoidforce * (m_lookAhead - dist) / m_lookAhead;
        }
        return Vector2.zero;
    }

}
