﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
    public Transform playerXform;

    void Start()
    {
        GameManager.Instance.PlayerCamera = this;
    }

    public void Init(Transform xform)
    {
        playerXform = xform;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerXform == null)
            return;

        transform.position = new Vector3(playerXform.position.x, playerXform.position.y, transform.position.z);
    }
}
