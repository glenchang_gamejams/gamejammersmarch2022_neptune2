using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UnitRewardFeedback : MonoBehaviour
{
    public int Reward;
    public Text m_rewardText;

    public void ShowFeedback(int reward)
    {
        Reward = reward;
        StopAllCoroutines();
        StartCoroutine(GoldRewardFeedbackRoutine());
    }

    private IEnumerator GoldRewardFeedbackRoutine()
    {
        m_rewardText.text = Reward.ToString();
        yield return new WaitForSeconds(0.8f);
        m_rewardText.text = "";
    }
}
