﻿public class PlanetGoldUpdated : TEventBase
{
    public int Amount;
    public int MaxGold;

    public PlanetGoldUpdated(int amount, int maxWater)
    {
        Amount = amount;
        MaxGold = maxWater;
    }
}

public class GameStateChanged : TEventBase
{
    public GameManager.GameState State;

    public GameStateChanged(GameManager.GameState state)
    {
        State = state;
    }
}

public class PlanetWaterDepleted : TEventBase { }

public class PlanetDataUpdated : TEventBase
{
    public PlanetData Data;

    public PlanetDataUpdated(PlanetData data) 
    {
        Data = data;
    }
}

public class PlayerDataUpdated : TEventBase
{
    public PlayerData Data;

    public PlayerDataUpdated(PlayerData data) 
    {
        Data = data;
    }
}

public class TurretDataUpdated : TEventBase
{
    public TurretData Data;

    public TurretDataUpdated(TurretData data)
    {
        Data = data;
    }
}

public class HangerDataUpdated : TEventBase
{
    public HangerData Data;

    public HangerDataUpdated(HangerData data)
    {
        Data = data;
    }
}

public class PlanetShieldDataUpdated : TEventBase
{
    public PlanetShieldData Data;

    public PlanetShieldDataUpdated(PlanetShieldData data)
    {
        Data = data;
    }
}

public class FleetDataUpdated : TEventBase
{
    public FleetData Data;

    public FleetDataUpdated(FleetData data)
    {
        Data = data;
    }
}

public class FighterDataUpdated : TEventBase
{
    public FighterData Data;

    public FighterDataUpdated(FighterData data)
    {
        Data = data;
    }
}

public class WeaponDataUpdated : TEventBase
{
    public WeaponData Data;

    public WeaponDataUpdated(WeaponData data)
    {
        Data = data;
    }
}

public class SpeedDataUpdated : TEventBase
{
    public SpeedData Data;

    public SpeedDataUpdated(SpeedData data)
    {
        Data = data;
    }
}

public class ShieldDataUpdated : TEventBase
{
    public ShieldData Data;

    public ShieldDataUpdated(ShieldData data)
    {
        Data = data;
    }
}

public class ShipDataUpdated : TEventBase
{
    public ShipData Data;

    public ShipDataUpdated(ShipData data)
    {
        Data = data;
    }
}

public class EnemyDestroyed : TEventBase
{
    public int Reward;

    public EnemyDestroyed(int reward)
    {
        Reward = reward;
    }
}



