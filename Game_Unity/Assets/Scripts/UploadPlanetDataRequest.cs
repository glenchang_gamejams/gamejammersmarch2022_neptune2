using System;
using Toolset.Core;
using Toolset.Networking;

public class UploadPlanetDataRequest : HttpRequest<PlanetData, NoData>
{
    public UploadPlanetDataRequest(PlanetData uploadData) : base(uploadData)
    {
        NetworkRequestSettings.RetryPolicy = RequestRetryPolicy.None;
    }

    protected override HttpRequestMethod HttpRequestMethod => HttpRequestMethod.Put;

    protected override Uri Url => new Uri(ToolsetConstants.c_remoteUrl.StringBuilderAppend("UploadPlanetData"));
}