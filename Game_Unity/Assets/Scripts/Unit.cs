using UnityEngine;

public abstract class Unit : MonoBehaviour
{
    protected Transform m_player;
    public Health Health;

    protected SpriteRenderer[] m_sprites;
    protected float m_startupTime = 2.0f;
    protected float m_timePassed = 0.0f;

    public bool IsEnemy = false;
    public int Reward = 0;

    virtual public void Init()
    {
        m_sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sprite in m_sprites)
        {
            var color = sprite.color;
            color.a = 1f;
            sprite.color = color;
            sprite.sortingLayerName = IsEnemy ? "Enemy" : "Player";
        }

        if (GameManager.Instance.PlayerController != null)
            m_player = GameManager.Instance.PlayerController.transform;

        // Set faction
        IsEnemy = !GameManager.Instance.IsAtHome;
        var factionLayer = GameManager.Instance.IsAtHome ? Layers.Player : Layers.Enemy;
        gameObject.layer = factionLayer;
        foreach(Transform child in transform)
        {
            child.gameObject.layer = factionLayer;
        }
    }

    private void OnEnable()
    {
        // Handle ObjectPool respawn properties.
        Init();
    }

    // Use this for initialization
    void Start()
    {
        Init();

        Health = GetComponent<Health>();
        Health?.FullHeal();

        Health.DiedEvent += OnDeath;
    }

    public void SetHealth(int max)
    {
        if (Health == null)
            Health = GetComponent<Health>();
        Health?.SetMaxHealth(max);
    }

    virtual public void ForceDeath()
    {
        Health.ForceDeath();
    }

    virtual protected void OnDeath(object sender, System.EventArgs e)
    {
        if (!IsEnemy)
            return;

        EventManager.Fire(new EnemyDestroyed(Reward));
    }
}
