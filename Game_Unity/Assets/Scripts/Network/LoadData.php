<?php
//You have to fill in this information to connect to your database!
$servername = "localhost";
$username = "gchang_games";
$password = "Uu%LNa]zWA=O";
$dbname = "gchang_game_neptuneII";

// Create connection
$mysqli = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($mysqli->connect_error) {
	die("Connection failed: " . $mysqli->connect_error);
} 

//These are our variables.
//We use real escape string to stop people from injecting. We handle this in Unity too, but it's important we do it here as well in case people extract our url.
$xPlanetName = $mysqli->real_escape_string($_GET['xPlanetName']); 

//This is the polite version of our name
$politestring = sanitize($xPlanetName);

//Here's our query to grab a rank.
$sql = "SELECT * FROM GameData WHERE xPlanetName = '$politestring'";

// Uncomment this to debug, but this will fail in game because the result returned is the echo rather than the result
//echo $sql;

$result = $mysqli->query($sql) or die('Query failed: ' . $con->error); 

if ($result)
{     
	$num_rows = $result->num_rows;  
	
	for($i = 0; $i < $num_rows; $i++)
	{
		$row = $result->fetch_array(MYSQLI_BOTH);
		echo $row['xPlanetName'] . "\t" . $row['UpgradeCost'] . "\t" . $row['GoldMax'] . "\t" . $row['Scale'] . "\t" . $row['xGoldCount'] . "\t" . $row['xTurretCount'] . "\t" . $row['xHangerCount'] . "\t" . $row['xPlanetShieldCount'] . "\t" . $row['xFleetCount'] . "\t" . $row['xFighterCount'] . "\t" . $row['xTurretLevel'] . "\t" . $row['xHangerLevel'] . "\t" . $row['xPlanetShieldLevel'] . "\t" . $row['xFleetLevel'] . "\t"  . $row['xFighterLevel'] . "\t". $row['xPlanetLevel'] . "\t" . $row['ShipRepairCost'] . "\t" . $row['xTimeStamp'] . "\t" . $row['xWeaponLevel'] . "\t" . $row['xShieldLevel'] . "\t" . $row['xSpeedLevel'] . "\t" . $row['xShipLevel'] . "\n"; 
	}
	$result->close();	
}


$mysqli->close();


/////////////////////////////////////////////////
// string sanitize functionality to avoid
// sql or html injection abuse and bad words
/////////////////////////////////////////////////
function no_naughty($string)
{
    $string = preg_replace('/shit/i', 'shoot', $string);
    $string = preg_replace('/fuck/i', 'fool', $string);
    $string = preg_replace('/asshole/i', 'animal', $string);
    $string = preg_replace('/bitches/i', 'dogs', $string);
    $string = preg_replace('/bitch/i', 'dog', $string);
    $string = preg_replace('/bastard/i', 'plastered', $string);
    $string = preg_replace('/nigger/i', 'newbie', $string);
    $string = preg_replace('/cunt/i', 'corn', $string);
    $string = preg_replace('/cock/i', 'rooster', $string);
    $string = preg_replace('/faggot/i', 'piglet', $string);

    $string = preg_replace('/suck/i', 'rock', $string);
    $string = preg_replace('/dick/i', 'deck', $string);
    $string = preg_replace('/crap/i', 'rap', $string);
    $string = preg_replace('/blows/i', 'shows', $string);

    // ie does not understand "&apos;" &#39; &rsquo;
    $string = preg_replace("/'/i", '&rsquo;', $string);
    $string = preg_replace('/%39/i', '&rsquo;', $string);
    $string = preg_replace('/&#039;/i', '&rsquo;', $string);
    $string = preg_replace('/&039;/i', '&rsquo;', $string);

    $string = preg_replace('/"/i', '&quot;', $string);
    $string = preg_replace('/%34/i', '&quot;', $string);
    $string = preg_replace('/&034;/i', '&quot;', $string);
    $string = preg_replace('/&#034;/i', '&quot;', $string);

    // these 3 letter words occur commonly in non-rude words...
    //$string = preg_replace('/fag', 'pig', $string);
    //$string = preg_replace('/ass', 'donkey', $string);
    //$string = preg_replace('/gay', 'happy', $string);
    return $string;
}

function my_utf8($string)
{
    return strtr($string,
      "/<>������������ ��Ց������������������������������ԕ���ٞ��������",
      "![]YuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");
}

function safe_typing($string)
{
    return preg_replace("/[^a-zA-Z0-9 \!\@\%\^\&\*\.\*\?\+\[\]\(\)\{\}\^\$\:\;\,\-\_\=]/", "", $string);
}

function sanitize($string)
{
    // make sure it isn't waaaaaaaay too long
    $MAX_LENGTH = 250; // bytes per chat or text message - fixme?
    $string = substr($string, 0, $MAX_LENGTH);
    $string = no_naughty($string);
    // breaks apos and quot: // $string = htmlentities($string,ENT_QUOTES);
    // useless since the above gets rid of quotes...
    //$string = str_replace("'","&rsquo;",$string);
    //$string = str_replace("\"","&rdquo;",$string);
    //$string = str_replace('#','&pound;',$string); // special case
    $string = my_utf8($string);
    $string = safe_typing($string);
    return trim($string);
}


?>