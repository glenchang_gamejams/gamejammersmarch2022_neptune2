<?php
//You have to fill in this information to connect to your database!
$servername = "localhost";
$username = "gchang_games";
$password = "Uu%LNa]zWA=O";
$dbname = "gchang_game_neptuneII";

// Create connection
$mysqli = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($mysqli->connect_error) {
	die("Connection failed: " . $mysqli->connect_error);
} 

//This is your key. You have to fill this in! Go and generate a strong one.
$secretKey="gbc052510180109";

//These are our variables.
//We use real escape string to stop people from injecting. We handle this in Unity too, but it's important we do it here as well in case people extract our url.
$xPlanetName = $mysqli->real_escape_string($_GET['xPlanetName']); 
$UpgradeCost = $mysqli->real_escape_string($_GET['UpgradeCost']); 
$GoldMax = $mysqli->real_escape_string($_GET['GoldMax']); 
$Scale = $mysqli->real_escape_string($_GET['Scale']); 
$xGoldCount = $mysqli->real_escape_string($_GET['xGoldCount']); 

$xTurretCount = $mysqli->real_escape_string($_GET['xTurretCount']); 
$xHangerCount = $mysqli->real_escape_string($_GET['xHangerCount']); 
$xPlanetShieldCount = $mysqli->real_escape_string($_GET['xPlanetShieldCount']); 
$xFleetCount = $mysqli->real_escape_string($_GET['xFleetCount']); 
$xFighterCount = $mysqli->real_escape_string($_GET['xFighterCount']); 

$xTurretLevel = $mysqli->real_escape_string($_GET['xTurretLevel']); 
$xHangerLevel = $mysqli->real_escape_string($_GET['xHangerLevel']); 
$xPlanetShieldLevel = $mysqli->real_escape_string($_GET['xPlanetShieldLevel']); 
$xFleetLevel = $mysqli->real_escape_string($_GET['xFleetLevel']); 
$xFighterLevel = $mysqli->real_escape_string($_GET['xFighterLevel']); 
$xPlanetLevel = $mysqli->real_escape_string($_GET['xPlanetLevel']); 

$ShipRepairCost = $mysqli->real_escape_string($_GET['ShipRepairCost']); 

$xTimeStamp = $mysqli->real_escape_string($_GET['xTimeStamp']); 
$xWeaponLevel = $mysqli->real_escape_string($_GET['xWeaponLevel']); 
$xShieldLevel = $mysqli->real_escape_string($_GET['xShieldLevel']); 
$xSpeedLevel = $mysqli->real_escape_string($_GET['xSpeedLevel']); 
$xShipLevel = $mysqli->real_escape_string($_GET['xShipLevel']); 

$hash = $_GET['hash']; 


//This is the polite version of our name
$politestring = sanitize($xPlanetName);

//We md5 hash our results.
$expected_hash = md5($xPlanetName . $xGoldCount . $secretKey); 

//If what we expect is what we have:
if($expected_hash == $hash) 
{ 
	// Here's our query to insert/update xGoldCount!
	$sql = "INSERT INTO GameData
    (xPlanetName,
    UpgradeCost,
    GoldMax,
    Scale,
    xGoldCount,

    xTurretCount,
    xHangerCount,
    xPlanetShieldCount,
    xFleetCount,
    xFighterCount,

    xTurretLevel,
    xHangerLevel,
    xPlanetShieldLevel,
    xFleetLevel,
    xFighterLevel,
    xPlanetLevel,

    ShipRepairCost,

    xTimeStamp,
    xWeaponLevel,
    xShieldLevel,
    xSpeedLevel,
    xShipLevel,

    ts)
    VALUES

    ('$politestring',
    '$UpgradeCost',
    '$GoldMax',
    '$Scale',
    '$xGoldCount',

    '$xTurretCount',
    '$xHangerCount',
    '$xPlanetShieldCount',
    '$xFleetCount',
    '$xFighterCount',

    '$xTurretLevel',
    '$xHangerLevel',
    '$xPlanetShieldLevel',
    '$xFleetLevel',
    '$xFighterLevel',
    '$xPlanetLevel',

    '$ShipRepairCost',

    '$xTimeStamp',
    '$xWeaponLevel',
    '$xShieldLevel',
    '$xSpeedLevel',
    '$xShipLevel',

	CURRENT_TIMESTAMP)

	ON DUPLICATE KEY 
    UPDATE
    UpgradeCost='$UpgradeCost',
    GoldMax='$GoldMax',
    Scale='$Scale',
    xGoldCount='$xGoldCount',

    xTurretCount='$xTurretCount',
    xHangerCount='$xHangerCount',
    xPlanetShieldCount='$xPlanetShieldCount',
    xFleetCount='$xFleetCount',
    xFighterCount='$xFighterCount',

    xTurretLevel='$xTurretLevel',
    xHangerLevel='$xHangerLevel',
    xPlanetShieldLevel='$xPlanetShieldLevel',
    xFleetLevel='$xFleetLevel',
    xFighterLevel='$xFighterLevel',
    xPlanetLevel='$xPlanetLevel',

    ShipRepairCost='$ShipRepairCost',

    xTimeStamp='$xTimeStamp',
    xWeaponLevel='$xWeaponLevel',
    xShieldLevel='$xShieldLevel',
    xSpeedLevel='$xSpeedLevel',
    xShipLevel='$xShipLevel'"; 

    echo $sql;
	//And finally we send our query.
	$result = $mysqli->query($sql) or die('Query failed: ' . $con->error);
}

$mysqli->close();


/////////////////////////////////////////////////
// string sanitize functionality to avoid
// sql or html injection abuse and bad words
/////////////////////////////////////////////////
function no_naughty($string)
{
    $string = preg_replace('/shit/i', 'shoot', $string);
    $string = preg_replace('/fuck/i', 'fool', $string);
    $string = preg_replace('/asshole/i', 'animal', $string);
    $string = preg_replace('/bitches/i', 'dogs', $string);
    $string = preg_replace('/bitch/i', 'dog', $string);
    $string = preg_replace('/bastard/i', 'plastered', $string);
    $string = preg_replace('/nigger/i', 'newbie', $string);
    $string = preg_replace('/cunt/i', 'corn', $string);
    $string = preg_replace('/cock/i', 'rooster', $string);
    $string = preg_replace('/faggot/i', 'piglet', $string);

    $string = preg_replace('/suck/i', 'rock', $string);
    $string = preg_replace('/dick/i', 'deck', $string);
    $string = preg_replace('/crap/i', 'rap', $string);
    $string = preg_replace('/blows/i', 'shows', $string);

    // ie does not understand "&apos;" &#39; &rsquo;
    $string = preg_replace("/'/i", '&rsquo;', $string);
    $string = preg_replace('/%39/i', '&rsquo;', $string);
    $string = preg_replace('/&#039;/i', '&rsquo;', $string);
    $string = preg_replace('/&039;/i', '&rsquo;', $string);

    $string = preg_replace('/"/i', '&quot;', $string);
    $string = preg_replace('/%34/i', '&quot;', $string);
    $string = preg_replace('/&034;/i', '&quot;', $string);
    $string = preg_replace('/&#034;/i', '&quot;', $string);

    // these 3 letter words occur commonly in non-rude words...
    //$string = preg_replace('/fag', 'pig', $string);
    //$string = preg_replace('/ass', 'donkey', $string);
    //$string = preg_replace('/gay', 'happy', $string);
    return $string;
}

function my_utf8($string)
{
    return strtr($string,
      "/<>������������ ��Ց������������������������������ԕ���ٞ��������",
      "![]YuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");
}

function safe_typing($string)
{
    return preg_replace("/[^a-zA-Z0-9 \!\@\%\^\&\*\.\*\?\+\[\]\(\)\{\}\^\$\:\;\,\-\_\=]/", "", $string);
}

function sanitize($string)
{
    // make sure it isn't waaaaaaaay too long
    $MAX_LENGTH = 250; // bytes per chat or text message - fixme?
    $string = substr($string, 0, $MAX_LENGTH);
    $string = no_naughty($string);
    // breaks apos and quot: // $string = htmlentities($string,ENT_QUOTES);
    // useless since the above gets rid of quotes...
    //$string = str_replace("'","&rsquo;",$string);
    //$string = str_replace("\"","&rdquo;",$string);
    //$string = str_replace('#','&pound;',$string); // special case
    $string = my_utf8($string);
    $string = safe_typing($string);
    return trim($string);
}

?>
