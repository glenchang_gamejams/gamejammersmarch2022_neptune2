﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Network : MonoBehaviour
{
    //The GUI Text object we're going to base everything off.
    public GameObject[] NeighborTextGroups;
    [SerializeField]
    private Color m_playerTextColor = new Color(1f, 0.5f, 0.5f);

    ///Fill in your server data here.
    private string m_privateKey = "gbc052510180109";

    private string GetNeighborDataURL = "https://www.glenchang.com/main/games/Neptune_II/GetNeighbors.php?";
    private string SaveDataURL = "https://glenchang.com/main/games/Neptune_II/SaveData.php?";
    private string LoadDataURL = "https://glenchang.com/main/games/Neptune_II/LoadData.php?";

    void Awake()
    {
        GameManager.Instance.Network = this;
    }

    ///Our encryption function: http://wiki.unity3d.com/index.php?title=MD5
    private string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    void Error()
    {
        Debug.Log("Error.");
    }

    //-----------------------------------------------------------------------------------------------------------------
    public void SaveData()
    {
        StartCoroutine(SaveDataRoutine()); // We post our scores.
        Debug.Log("Saving Data " + GameManager.Instance.PlayerPlanetData.xPlanetName);
    }

    //-----------------------------------------------------------------------------------------------------------------
    public IEnumerator SaveDataRoutine()
    {
        PlanetData planetData = GameManager.Instance.PlayerPlanetData;
        PlayerData playerData = GameManager.Instance.PlayerData;

        string xPlanetName = planetData.xPlanetName;
        int xGoldCount = planetData.xGoldCount;

        string hash = Md5Sum(xPlanetName + xGoldCount + m_privateKey);

        string saveString = SaveDataURL
            + "xPlanetName=" + WWW.EscapeURL(xPlanetName)
            + "&" + "UpgradeCost=" + planetData.UpgradeCost
            + "&" + "GoldMax=" + planetData.GoldMax
            + "&" + "Scale=" + planetData.Scale
            + "&" + "xGoldCount=" + xGoldCount 
            + "&" + "xTurretCount=" + planetData.xTurretCount
            + "&" + "xHangerCount=" + planetData.xHangerCount 
            + "&" + "xPlanetShieldCount=" + planetData.xShieldCount 
            + "&" + "xFleetCount=" + planetData.xFleetCount 
            + "&" + "xFighterCount=" + planetData.xFighterCount 
            + "&" + "xTurretLevel=" + planetData.xTurretLevel
            + "&" + "xHangerLevel=" + planetData.xHangerLevel 
            + "&" + "xPlanetShieldLevel=" + planetData.xShieldLevel 
            + "&" + "xFleetLevel=" + planetData.xFleetLevel 
            + "&" + "xFighterLevel=" + planetData.xFighterLevel 
            + "&" + "xPlanetLevel=" + planetData.xPlanetLevel 
            + "&" + "ShipRepairCost=" + planetData.ShipRepairCost
            + "&" + "xTimeStamp=" + playerData.xTimeStamp 
            + "&" + "xWeaponLevel=" + playerData.xWeaponLevel
            + "&" + "xShieldLevel=" + playerData.xShieldLevel
            + "&" + "xSpeedLevel=" + playerData.xSpeedLevel
            + "&" + "xShipLevel=" + playerData.xShipLevel
            + "&hash=" + hash
            + $"&noCacheRandom={UnityEngine.Random.Range(0f, float.MaxValue)}";

        Debug.Log(saveString);
        using (UnityWebRequest webRequest = UnityWebRequest.Get(saveString))
        {
            webRequest.useHttpContinue = false;
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Web request error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("Web request HTTP error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log($"Saved data for {planetData.xPlanetName}");
                    break;
            }
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    public void LoadData()
    {
        //StartCoroutine(LoadDataRoutine()); // We post our scores.
        //Debug.Log("Loading Data ");
    }

    //-----------------------------------------------------------------------------------------------------------------
    public IEnumerator LoadDataRoutine(string name)
    {
        string loadString = LoadDataURL + "xPlanetName=" + WWW.EscapeURL(name) + $"&noCacheRandom={UnityEngine.Random.Range(0f, float.MaxValue)}";
        Debug.Log(loadString);
        using (UnityWebRequest webRequest = UnityWebRequest.Get(loadString))
        {

            webRequest.useHttpContinue = false;
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Web request error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("Web request HTTP error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    string result = webRequest.downloadHandler.text;
                    Debug.Log($"Result :{result}");

                    // Parse data
                    string[] tokens = result.Split('\t');
                    if (tokens.Length != 22)
                    {
                        Debug.LogWarning($"Token size is :{tokens.Length}. Data doesn't exist for {name}");
                        yield break;
                    }

                    int i = 1;
                    var planetData = new PlanetData();
                    planetData.PopulateFrom(StaticData.GetPlanetData());

                    planetData.xPlanetName = name;
                    planetData.UpgradeCost = Int32.Parse(tokens[i++]);
                    planetData.GoldMax = Int32.Parse(tokens[i++]);
                    planetData.Scale = Int32.Parse(tokens[i++]);
                    planetData.xGoldCount = Int32.Parse(tokens[i++]);

                    planetData.xTurretCount = Int32.Parse(tokens[i++]);
                    planetData.xHangerCount = Int32.Parse(tokens[i++]);
                    planetData.xShieldCount = Int32.Parse(tokens[i++]);
                    planetData.xFleetCount = Int32.Parse(tokens[i++]);
                    planetData.xFighterCount = Int32.Parse(tokens[i++]);

                    planetData.xTurretLevel = Int32.Parse(tokens[i++]);
                    planetData.xHangerLevel = Int32.Parse(tokens[i++]);
                    planetData.xShieldLevel = Int32.Parse(tokens[i++]);
                    planetData.xFleetLevel = Int32.Parse(tokens[i++]);
                    planetData.xFighterLevel = Int32.Parse(tokens[i++]);
                    planetData.xPlanetLevel = Int32.Parse(tokens[i++]);

                    planetData.ShipRepairCost = Int32.Parse(tokens[i++]);

                    var playerData = new PlayerData();
                    playerData.PopulateFrom(StaticData.GetPlayerData());

                    playerData.xTimeStamp = Int32.Parse(tokens[i++]);
                    playerData.xWeaponLevel = Int32.Parse(tokens[i++]);
                    playerData.xShieldLevel = Int32.Parse(tokens[i++]);
                    playerData.xSpeedLevel = Int32.Parse(tokens[i++]);
                    playerData.xShipLevel = Int32.Parse(tokens[i++]);

                    GameManager.Instance.PlayerPlanetData = planetData;
                    GameManager.Instance.PlayerData = playerData;

                    Debug.Log($"Loaded Data for {planetData.xPlanetName}");
                    yield break;
            }
        }
    }

    public void GetNeighbors()
    {
        StartCoroutine(GetNeighborsRoutine());
    }

    //-----------------------------------------------------------------------------------------------------------------
    public IEnumerator GetNeighborsRoutine()
    {
        name = GameManager.Instance.PlayerPlanetData.xPlanetName;
        string loadString = GetNeighborDataURL + "xPlanetName=" + WWW.EscapeURL(name) + $"&noCacheRandom={UnityEngine.Random.Range(0f, float.MaxValue)}";
        Debug.Log(loadString);
        using (UnityWebRequest webRequest = UnityWebRequest.Get(loadString))
        {
            webRequest.useHttpContinue = false;
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Web request error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("Web request HTTP error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    string result = webRequest.downloadHandler.text;
                    Debug.Log($"Get Neighbors Result :\n{result}");

                    if (string.IsNullOrEmpty(result))
                    {
                        Debug.LogWarning($"Failed to retrieve neighbors, empty web request {result}");
                        yield break;
                    }

                    // Dirty way to show neighbors in UI
                    // Clear list
                    foreach(GameObject neighborTextGrp in NeighborTextGroups)
                    {
                        Text[] planetText = neighborTextGrp.GetComponentsInChildren<Text>();
                        planetText[0].text = "";
                        planetText[1].text = "";
                    }

                    int neighborTextIndex = 0;

                    // Parse data
                    GameManager.Instance.Neighbors.Clear();
                    string[] planetTokens = result.Split('\n');
                    foreach(string planet in planetTokens)
                    {
                        string[] tokens = planet.Split('\t');
                        if (tokens.Length != 22)
                        {
                            Debug.LogWarning($"Token size is :{tokens.Length}. Data doesn't exist for planet data {planet}");
                            continue;
                        }

                        // ---------------------------------
                        // Populate list
                        if (neighborTextIndex < NeighborTextGroups.Length)
                        {
                            // Dirty way to show neighbors in UI
                            Text[] planetText = NeighborTextGroups[neighborTextIndex].GetComponentsInChildren<Text>();
                            planetText[0].text = tokens[0];
                            planetText[1].text = tokens[4];

                            // If nieghbor is player, skip
                            if (tokens[0] == GameManager.Instance.HomePlanet.PlanetData.xPlanetName)
                            {
                                planetText[0].fontStyle = FontStyle.Bold;
                                planetText[1].fontStyle = FontStyle.Bold;
                                planetText[0].color = m_playerTextColor;
                            }
                            else
                            {
                                planetText[0].fontStyle = FontStyle.Normal;
                                planetText[1].fontStyle = FontStyle.Normal;
                                planetText[0].color = Color.black;
                            }

                            neighborTextIndex++;
                        }

                        // ---------------------------------

                        // If nieghbor is player, skip
                        if (tokens[0] == GameManager.Instance.HomePlanet.PlanetData.xPlanetName)
                        {
                            continue;
                        }

                        var planetData = new PlanetData();
                        planetData.PopulateFrom(StaticData.GetPlanetData());

                        int i = 0;
                        planetData.xPlanetName = tokens[i++];
                        planetData.UpgradeCost = Int32.Parse(tokens[i++]);
                        planetData.GoldMax = Int32.Parse(tokens[i++]);
                        planetData.Scale = Int32.Parse(tokens[i++]);
                        planetData.xGoldCount = Int32.Parse(tokens[i++]);

                        planetData.xTurretCount = Int32.Parse(tokens[i++]);
                        planetData.xHangerCount = Int32.Parse(tokens[i++]);
                        planetData.xShieldCount = Int32.Parse(tokens[i++]);
                        planetData.xFleetCount = Int32.Parse(tokens[i++]);
                        planetData.xFighterCount = Int32.Parse(tokens[i++]);

                        planetData.xTurretLevel = Int32.Parse(tokens[i++]);
                        planetData.xHangerLevel = Int32.Parse(tokens[i++]);
                        planetData.xShieldLevel = Int32.Parse(tokens[i++]);
                        planetData.xFleetLevel = Int32.Parse(tokens[i++]);
                        planetData.xFighterLevel = Int32.Parse(tokens[i++]);
                        planetData.xPlanetLevel = Int32.Parse(tokens[i++]);

                        planetData.ShipRepairCost = Int32.Parse(tokens[i++]);

                        var playerData = new PlayerData();
                        playerData.PopulateFrom(StaticData.GetPlayerData());

                        playerData.xTimeStamp = Int32.Parse(tokens[i++]);
                        playerData.xWeaponLevel = Int32.Parse(tokens[i++]);
                        playerData.xShieldLevel = Int32.Parse(tokens[i++]);
                        playerData.xSpeedLevel = Int32.Parse(tokens[i++]);
                        playerData.xShipLevel = Int32.Parse(tokens[i++]);

                        GameManager.Instance.Neighbors.Add(planetData);

                        string message = $"Retreived Neighbor PlanetData : {planetData.PlanetDataId}\n";
                        message += $"Planet Name : {planetData.xPlanetName}\n";
                        message += $"Upgrade Cost : {planetData.UpgradeCost}\n";
                        message += $"Turrets : {planetData.xTurretCount}\n";
                        message += $"Turret Lvl : {planetData.xTurretLevel}\n";
                        message += $"Fighters : {planetData.xFighterCount}\n";
                        message += $"Fighters Lvl : {planetData.xFighterLevel}\n";
                        message += $"Hangers : {planetData.xHangerCount}\n";
                        message += $"Hangers Lvl : {planetData.xHangerLevel}\n";
                        message += $"Shields : {planetData.xShieldCount}\n";
                        message += $"Shields Lvl : {planetData.xShieldLevel}\n";
                        message += $"Fleet : {planetData.xFleetCount}\n";
                        message += $"Fleet Lvl : {planetData.xFleetLevel}\n";
                        Debug.Log(message);
                    }

                    yield break;
            }

            yield return new WaitForSeconds(1f);

            // Dirty way to update player gold
            // Clear list
            foreach (GameObject neighborTextGrp in NeighborTextGroups)
            {
                Text[] planetText = neighborTextGrp.GetComponentsInChildren<Text>();
                if (planetText[0].text == GameManager.Instance.PlayerPlanetData.xPlanetName)
                    planetText[1].text = GameManager.Instance.PlayerPlanetData.xGoldCount.ToString();
            }
        }
    }
}