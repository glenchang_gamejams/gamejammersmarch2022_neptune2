﻿using System;
using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public WeaponData WeaponData { get; protected set; }
    public ProjectileData ProjectileData { get; protected set; }
    
    private float fireNext = 0.0f;

    public enum hitLayerEnum {CanHitEnemy, CanHitPlayer, HitAll};
    public hitLayerEnum hitLayer;

    public GameObject projectilePrefab;

    public void PopulateFromStaticData(string weaponDataId = StaticData.c_defaultId, string projectileDataId = StaticData.c_defaultId)
    {
        WeaponData = StaticData.GetWeaponData(weaponDataId);
        ProjectileData = StaticData.GetProjectileData(StaticData.c_defaultId);
    }

    virtual public void FireProjectile(Transform target)
    {
        // This should later be converted to a function to recycle instances
        GameObject projectileObj = ObjectPoolManager.Instance.Spawn(projectilePrefab, transform.position + -transform.up, transform.rotation) as GameObject;
        Projectile projectile = projectileObj.GetComponent<Projectile>();
        
        if (projectile != null)
        {
            projectile.SetStaticData(ProjectileData);

            // Place the projectile into the correct layer
            if (hitLayer == hitLayerEnum.CanHitEnemy)
                projectileObj.layer = Layers.CanHitEnemy;
            else if (hitLayer == hitLayerEnum.CanHitPlayer)
                projectileObj.layer = Layers.CanHitPlayer;
            // else it hits everything

            projectile.SetVelocity(-transform.up * WeaponData.ProjectileSpeed);
            projectile.transform.rotation = Quaternion.FromToRotation(Vector3.up, -transform.up);
        }
        var health = projectileObj.GetComponent<Health>();
        if (health != null)
            health.FullHeal();
    }

    // Fires this weapon
    public void Fire(Transform target)
    {
        if (Time.time > fireNext)
        {
            fireNext = Time.time + WeaponData.FireRate;
            FireProjectile(target);
        }
    }
}
