﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerShip : Ship
{
    // Main Weapon
    public GameObject m_mainWeaponPrefab;
    public Transform m_mainWeaponHP;

    // Shields
    public GameObject m_shieldPrefab;
    public int m_numShields = 0;
    public float m_shieldRegenRate = 5f;
    private float m_shieldRegenNext;
    public Transform[] m_shieldHardpoints;
    
    // Speed Upgrade
    public GameObject m_speedPrefab;
    public float[] m_speedMass;

    // Water Tank Upgrade
    public GameObject m_waterTankPrefab;
    public GameObject m_shipBase;

    // Player color
    public Color m_playerColor = new Color(172f, 74f, 255f);

    public ParticleSystem m_waterPullEffect;
    //private bool m_pullingWater = false;
    public float m_maxWater = 500.0f;
    public float m_water = 0;
    public float m_waterDrainRate = 100.0f;
    public float m_waterDrainRange = 5.0f;
    public Image m_waterTankImage;

	//PullWater Audio
	public AudioClip m_pullWaterSound;
	private AudioSource m_source;

    // Player ship color
    public Color m_shipColor = Color.blue;
    public float CombatRange = 20f;
    public Rigidbody2D m_rigidBody2d;

    // Player Health
    private Health Health;

    public WeaponData WeaponData;
    public ShieldData ShieldData;
    public SpeedData SpeedData;
    public ShipData ShipData;

    public bool IsDead => Health.IsDead;

    private void Awake()
    {
        gameObject.layer = Layers.Player;
        m_rigidBody2d = GetComponent<Rigidbody2D>();

        Health = GetComponent<Health>();
        Health.DiedEvent += OnDeath;
    }
    virtual protected void OnDeath(object sender, System.EventArgs e)
    {
        GameManager.Instance.FailedInvasion();
    }

    public void SetData(PlayerData playerData)
    {
        // TODO : Should size with planet
        UpgradeWeapon(playerData.xWeaponLevel, playerData.WeaponDataId);
        UpgradeShield(playerData.xShieldLevel, playerData.ShieldDataId);
        UpgradeSpeed(playerData.xSpeedLevel, playerData.SpeedDataId);
        UpgradeShip(playerData.xShipLevel, playerData.ShipDataId);

    }

    public void UpgradeWeapon(int level, string id)
    {
        WeaponData = StaticData.GetWeaponData(id);
        EventManager.Fire(new WeaponDataUpdated(WeaponData));

        var prefab = GameManager.Instance.Shop.m_weaponUpgrades[level];

        // Destroy the weapon that already exists
        foreach (Transform child in m_mainWeaponHP.transform)
            Destroy(child.gameObject);

        // Instantiate and attach main weapon to hardpoint
        GameObject mainWeaponObj = Instantiate(prefab, m_mainWeaponHP, false);
        Weapon[] weapons = mainWeaponObj.GetComponentsInChildren<Weapon>();

        foreach(var weapon in weapons)
        {
            weapon.PopulateFromStaticData(WeaponData.WeaponDataId, WeaponData.ChildProjectileDataId);
        }

        // Set sorting layer
        foreach(var spriteRenderer in mainWeaponObj.GetComponentsInChildren<SpriteRenderer>())
        {
            spriteRenderer.sortingLayerName = "Player";
        }

        mainWeaponObj.transform.localPosition = Vector3.zero;
        foreach (Transform xform in mainWeaponObj.transform)
            xform.gameObject.layer = Layers.Player;

        UpdateColor();
    }

    public void UpgradeShield(int level, string id)
    {
        ShieldData = StaticData.GetShieldData(id);
        EventManager.Fire(new ShieldDataUpdated(ShieldData));

        m_shieldPrefab = GameManager.Instance.Shop.m_shieldUpgrades[level];
        m_numShields = level;
        DestroyAllShields();
    }

    public void UpgradeSpeed(int level, string id)
    {
        SpeedData = StaticData.GetSpeedData(id);
        EventManager.Fire(new SpeedDataUpdated(SpeedData));

        m_speedPrefab = GameManager.Instance.Shop.m_speedUpgrades[level];

        if (level < m_speedMass.Length)
            m_rigidBody2d.mass = m_speedMass[level];

        // Destroy the speed
        Transform speed_HP = transform.Find("HP_Speed");
        foreach (Transform child in speed_HP.transform)
            Destroy(child.gameObject);

        // Instantiate and attach to hardpoint
        GameObject speedObj = Instantiate(m_speedPrefab, speed_HP, false) as GameObject;
        speedObj.transform.localPosition = Vector3.zero;
        if (speedObj != null)
            speedObj.layer = Layers.Player;

        UpdateColor();
    }

    public void UpgradeShip(int level, string id)
    {
        ShipData = StaticData.GetShipData(id);
        EventManager.Fire(new ShipDataUpdated(ShipData));
        Health.SetMaxHealth(ShipData.Health);
    }

    public void UpdateColor()
    {
        var sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sprite in sprites)
        {
            sprite.color = m_shipColor;
        }
    }

    public void DestroyAllShields()
    {
        // find a shield hardpoint without a child
        foreach(Transform hp in m_shieldHardpoints)
        {
            if (hp == null)
                continue;

            foreach (Transform t in hp)
                Destroy(t.gameObject);
        }
    }

    void GenerateShield()
    {
        // find a shield hardpoint without a child
        for(int i=0; i<m_numShields; i++)
        {
            Transform hp = m_shieldHardpoints[i];

            if (hp == null)
                continue;
            
            if (hp.childCount == 0)
            {
                GameObject shieldObj = Instantiate(m_shieldPrefab, hp, false) as GameObject;
                if (shieldObj != null)
                {
                    shieldObj.layer = Layers.Player;
                    UpdateColor();
                }
                return;
            }
        }
    }

    public void EquipWaterTank()
    {
        // Destroy the tank that exist
        Transform waterTank_HP = transform.Find("HP_Base");
        foreach (Transform child in waterTank_HP.transform)
            Destroy(child.gameObject);

        // Instantiate and attach to hardpoint
        m_shipBase = Instantiate(m_waterTankPrefab, waterTank_HP, false) as GameObject;
        m_shipBase.transform.localPosition = Vector3.zero;
        if (m_shipBase != null)
        {
            m_shipBase.layer = Layers.Player;
        }
        UpdateColor();

        // Update tank storage
        var shipTank = m_shipBase.GetComponent<ShipTank>();
        m_maxWater = shipTank.maxWater;
        m_waterDrainRate = shipTank.waterDrainRate;
        m_waterDrainRange = shipTank.waterDrainRange;
    }

    public void FireMainWeapon()
    {
        Weapon[] weapons = m_mainWeaponHP.GetComponentsInChildren<Weapon>();
        foreach(Weapon weap in weapons)
            weap.Fire(null);
    }
    
    public void PullWaterStart()
    {
		//Find AudioSource and play
		m_source = GetComponent<AudioSource> ();
		m_source.Play();
		m_source.loop = true;

        var shipTank = m_shipBase.GetComponent<ShipTank>();
        shipTank.OpenTank();

        // show water drain
        transform.Find("WaterDrain").gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update ()
    {
        if (ShipData == null)
            return;
        
        if (Time.time > m_shieldRegenNext)
        {
            // TODO : Replace 1.0f with shield data regen rate
            m_shieldRegenNext = Time.time + 1.0f;
            GenerateShield();
        }

        // Show how much water we have in the tank. This should be left in the update in case we need to use water for resource.
        if (m_waterTankImage)
        {
            m_waterTankImage.fillAmount = m_water / m_maxWater;
        }
	}

    void FixedUpdate()
    {
        if (transform.position.magnitude > CombatRange)
        {
            m_rigidBody2d.AddForce((Vector3.zero - transform.position).normalized * SpeedData.Speed);
        }
    }
}
