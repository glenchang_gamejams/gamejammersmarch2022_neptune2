﻿using UnityEngine;

public class Hanger : Unit {
    public Weapon m_weapon;
    public float m_releaseRateInSec = 2f;
    public float m_nextFighterReleaseTime;
    public int m_fighterCountMax;
    public int m_fighterCount;
    public GameObject m_fighterPrefab;

    public HangerData m_hangerData;

    public void SetData(HangerData hangerData)
    {
        m_hangerData = hangerData;
        m_startupTime = hangerData.StartupTime;
        m_releaseRateInSec = hangerData.ShipReleaseRateInSec;
        m_fighterCountMax = hangerData.MaxFighterCount;
        m_startupTime = hangerData.StartupTime;

        // To offset other unit fire times
        m_timePassed = Random.Range(0.0f, 3f);

        m_fighterCount = 0;

        Reward = (int)(hangerData.Cost * GameManager.Instance.m_attackRewardScalar);

        SetHealth(hangerData.Health);
    }

    // Use this for initialization
    void Start()
    {
        Init();

        Health = GetComponent<Health>();
        Health?.FullHeal();

        Health.DiedEvent += OnDeath;
    }

    void Update()
    {
        // Enemy behaviour
        if (!IsEnemy) 
            return;

        m_timePassed += Time.deltaTime;
        if (m_timePassed < m_startupTime)
        {
            if (m_sprites != null)
            {
                foreach (var sprite in m_sprites)
                {
                    var color = sprite.color;
                    color.a = m_timePassed / m_startupTime;
                    sprite.color = color;
                }
            }
            return;
        }

        // Release fighters
        if (m_timePassed >= m_nextFighterReleaseTime && m_fighterCount < m_fighterCountMax)
        {
            GameObject fighterObj = ObjectPoolManager.Instance.Spawn(m_fighterPrefab, transform.position + transform.up, transform.rotation);
            fighterObj.layer = gameObject.layer;

            Fighter fighter = fighterObj.GetComponent<Fighter>();
            fighter.SetData(GameManager.Instance.HomePlanet.FighterData);
            fighter.IsEnemy = IsEnemy;
            fighter.Health.DiedEvent += OnFighterDeath;
            fighter.Reward = 0;

            m_fighterCount++;
            m_nextFighterReleaseTime = m_timePassed + m_releaseRateInSec;
        }
    }

    private void OnFighterDeath(object sender, System.EventArgs e)
    {
        if (m_fighterCount > 0)
            m_fighterCount -= 1;
    }

    private float TransformAngle(float angle)
    {
        while (angle < 0.0f)
        {
            angle += 360.0f;
        }
        while (angle > 360.0f)
        {
            angle -= 360.0f;
        }

        return angle;
    }
}
