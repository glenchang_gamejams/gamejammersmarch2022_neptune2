﻿using UnityEngine;
using System.Collections;

public class OnDeathPlayer : MonoBehaviour
{
	public AudioClip deathSound;
    public GameObject explosionPrefab;

    //private GameDirector gameDirector;
	private float volLowRange = 10.0f;
	private float volHighRange = 15.0f;

    // Use this for initialization
    void Start ()
    {
        // Handler for death
        Health shipHealth = GetComponent<Health>();
        if(shipHealth!= null)
            shipHealth.DiedEvent += OnDeath;
    }
	
    public void OnDeath(object sender, System.EventArgs e)
    {
        if (explosionPrefab != null)
        {
            GameObject explosionObj = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosionObj.transform.localScale = transform.localScale;
			AudioSource source = explosionObj.GetComponent<AudioSource> ();
			if (source != null) 
			{
				float vol = Random.Range (volLowRange, volHighRange);
				source.PlayOneShot (deathSound,vol);
			}
        }
        ObjectPoolManager.Instance.Despawn(gameObject);
    }
}