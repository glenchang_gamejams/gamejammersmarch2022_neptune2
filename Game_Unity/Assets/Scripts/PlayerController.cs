﻿using Toolset.ProtocolBuffers;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameManager m_gameManager;

    // ----------------------------------------------------
    // Movement
    // ----------------------------------------------------
    private Rigidbody2D m_playerRigidBody;

    private float m_horizontalSpeed;
    private float m_verticalSpeed; 
    public bool CanMove = true;
    private bool m_isInitialized = false;

    // ----------------------------------------------------
    // Ship Weapons
    // ----------------------------------------------------
    private PlayerShip playerShip;

    private void Awake()
    {
        m_gameManager = GameManager.Instance;
        m_gameManager.PlayerController = this;

        playerShip = GetComponent<PlayerShip>();
        m_playerRigidBody = GetComponent<Rigidbody2D>();

        m_isInitialized = true;
    }

    void Update()
    {
        if (!m_isInitialized || !CanMove)
            return;

        // Mouse aim
        Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle + 90f, Vector3.forward);

        if (Input.GetButton("Fire1"))
        {
            playerShip.FireMainWeapon();
        }
    }

    void FixedUpdate()
    {
        if (!m_isInitialized || !CanMove)
            return;

        m_horizontalSpeed = Input.GetAxis("Horizontal");
        m_verticalSpeed = Input.GetAxis("Vertical");

        // TODO : Replace 20f with player data speed
        m_playerRigidBody.AddForce(new Vector3(m_horizontalSpeed * 20f, m_verticalSpeed * 20f, 0f));
    }

    public void ParkHome()
    {
        m_playerRigidBody.velocity = Vector3.zero;
        transform.position = GameManager.Instance.HomePlanet.transform.position - new Vector3(0, 0, 1);
        transform.rotation = Quaternion.identity;
    }

    public void SetupInvade(PlanetData data)
    {
        m_playerRigidBody.velocity = Vector3.zero;
        float combatRange = data.Scale * 2.0f;
        playerShip.CombatRange = combatRange;
        transform.position = GameManager.Instance.HomePlanet.transform.position - new Vector3(combatRange * 0.5f, 0f, 1f);
        transform.rotation = Quaternion.identity;
    }
}
