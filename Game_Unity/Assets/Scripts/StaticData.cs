using System;
using System.Collections;
using System.Collections.Generic;
using Toolset.Core;
using Toolset.ProtocolBuffers;
using UnityEngine;

public class StaticData : ToolsetMonoBehaviorSingleton<StaticData>
{
    public const string c_defaultId = "Default";
    private static readonly Dictionary<string, PlanetData> s_planetData = new Dictionary<string, PlanetData>();
    private static readonly Dictionary<string, PlayerData> s_playerData = new Dictionary<string, PlayerData>();

    private static readonly Dictionary<string, TurretData> s_turretData = new Dictionary<string, TurretData>();
    private static readonly Dictionary<string, HangerData> s_hangerData = new Dictionary<string, HangerData>();
    private static readonly Dictionary<string, FighterData> s_fighterData = new Dictionary<string, FighterData>();
    private static readonly Dictionary<string, PlanetShieldData> s_planetShieldData = new Dictionary<string, PlanetShieldData>();
    private static readonly Dictionary<string, FleetData> s_fleetData = new Dictionary<string, FleetData>();
    
    private static readonly Dictionary<string, WeaponData> s_weaponData = new Dictionary<string, WeaponData>();
    private static readonly Dictionary<string, ShieldData> s_shieldData = new Dictionary<string, ShieldData>();
    private static readonly Dictionary<string, SpeedData> s_speedData = new Dictionary<string, SpeedData>();
    private static readonly Dictionary<string, ShipData> s_shipData = new Dictionary<string, ShipData>();
    private static readonly Dictionary<string, ProjectileData> s_projectileData = new Dictionary<string, ProjectileData>();

protected override void Awake()
    {
        base.Awake();
        List<PlanetData> planetDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<PlanetData>();
        List<PlayerData> playerDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<PlayerData>();

        List<TurretData> turretDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<TurretData>();
        List<HangerData> hangerDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<HangerData>();
        List<FighterData> fighterDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<FighterData>();
        List<PlanetShieldData> planetShieldDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<PlanetShieldData>();
        List<FleetData> fleetDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<FleetData>();

        List<WeaponData> weaponDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<WeaponData>();
        List<ShieldData> shieldDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<ShieldData>();
        List<SpeedData> speedDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<SpeedData>();
        List<ShipData> shipDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<ShipData>();
        List<ProjectileData> projectileDataList = ProtoJsonConverter.DeserializeJsonModelsOfType<ProjectileData>();

        foreach (var item in planetDataList)
        {
            s_planetData.Add(item.PlanetDataId, item);
        }

        foreach (var item in playerDataList)
        {
            s_playerData.Add(item.PlayerDataId, item);
        }

        foreach (var item in turretDataList)
        {
            s_turretData.Add(item.TurretDataId, item);
        }

        foreach (var item in hangerDataList)
        {
            s_hangerData.Add(item.HangerDataId, item);
        }

        foreach (var item in fighterDataList)
        {
            s_fighterData.Add(item.FighterDataId, item);
        }

        foreach (var item in planetShieldDataList)
        {
            s_planetShieldData.Add(item.PlanetShieldDataId, item);
        }

        foreach (var item in fleetDataList)
        {
            s_fleetData.Add(item.FleetDataId, item);
        }

        foreach (var item in weaponDataList)
        {
            s_weaponData.Add(item.WeaponDataId, item);
        }
        
        foreach (var item in shieldDataList)
        {
            s_shieldData.Add(item.ShieldDataId, item);
        }
        
        foreach (var item in speedDataList)
        {
            s_speedData.Add(item.SpeedDataId, item);
        }
        
        foreach (var item in shipDataList)
        {
            s_shipData.Add(item.ShipDataId, item);
        }
        
        foreach (var item in projectileDataList)
        {
            s_projectileData.Add(item.ProjectileDataId, item);
        }
        
    }

    public static PlanetData GetPlanetData(string identifier = c_defaultId)
    {
        return s_planetData[identifier];
    }
    
    public static bool TryGetPlanetData(string identifier, out PlanetData output)
    {
        bool result = s_planetData.TryGetValue(identifier, out PlanetData internalResult);
        output = internalResult;
        return result;
    }

    public static PlayerData GetPlayerData(string identifier = c_defaultId)
    {
        return s_playerData[identifier];
    }

    public static bool TryGetPlayerData(string identifier, out PlayerData output)
    {
        bool result = s_playerData.TryGetValue(identifier, out PlayerData internalResult);
        output = internalResult;
        return result;
    }

    public static TurretData GetTurretData(string identifier = c_defaultId)
    {
        return s_turretData[identifier];
    }

    public static bool TryGetTurretData(string identifier, out TurretData output)
    {
        bool result = s_turretData.TryGetValue(identifier, out TurretData internalResult);
        output = internalResult;
        return result;
    }

    public static HangerData GetHangerData(string identifier = c_defaultId)
    {
        return s_hangerData[identifier];
    }

    public static bool TryGetHangerData(string identifier, out HangerData output)
    {
        bool result = s_hangerData.TryGetValue(identifier, out HangerData internalResult);
        output = internalResult;
        return result;
    }

    public static FighterData GetFighterData(string identifier = c_defaultId)
    {
        return s_fighterData[identifier];
    }

    public static bool TryGetFighterData(string identifier, out FighterData output)
    {
        bool result = s_fighterData.TryGetValue(identifier, out FighterData internalResult);
        output = internalResult;
        return result;
    }

    public static PlanetShieldData GetPlanetShieldData(string identifier = c_defaultId)
    {
        return s_planetShieldData[identifier];
    }

    public static bool TryGetPlanetShieldData(string identifier, out PlanetShieldData output)
    {
        bool result = s_planetShieldData.TryGetValue(identifier, out PlanetShieldData internalResult);
        output = internalResult;
        return result;
    }

    public static FleetData GetFleetData(string identifier = c_defaultId)
    {
        return s_fleetData[identifier];
    }

    public static bool TryGetFleetData(string identifier, out FleetData output)
    {
        bool result = s_fleetData.TryGetValue(identifier, out FleetData internalResult);
        output = internalResult;
        return result;
    }

    public static WeaponData GetWeaponData(string identifier = c_defaultId)
    {
        return s_weaponData[identifier];
    }
    
    public static bool TryGetWeaponData(string identifier, out WeaponData output)
    {
        bool result = s_weaponData.TryGetValue(identifier, out WeaponData internalResult);
        output = internalResult;
        return result;
    }
    
    public static ShieldData GetShieldData(string identifier = c_defaultId)
    {
        return s_shieldData[identifier];
    }
    
    public static bool TryGetShieldData(string identifier, out ShieldData output)
    {
        bool result = s_shieldData.TryGetValue(identifier, out ShieldData internalResult);
        output = internalResult;
        return result;
    }
    
    public static SpeedData GetSpeedData(string identifier = c_defaultId)
    {
        return s_speedData[identifier];
    }
    
    public static bool TryGetSpeedData(string identifier, out SpeedData output)
    {
        bool result = s_speedData.TryGetValue(identifier, out SpeedData internalResult);
        output = internalResult;
        return result;
    }
    
    public static ShipData GetShipData(string identifier = c_defaultId)
    {
        return s_shipData[identifier];
    }

    public static bool TryGetShipData(string identifier, out ShipData output)
    {
        bool result = s_shipData.TryGetValue(identifier, out ShipData internalResult);
        output = internalResult;
        return result;
    }
    
    public static ProjectileData GetProjectileData(string identifier = c_defaultId)
    {
        return s_projectileData[identifier];
    }
    
    public static bool TryGetProjectileData(string identifier, out ProjectileData output)
    {
        bool result = s_projectileData.TryGetValue(identifier, out ProjectileData internalResult);
        output = internalResult;
        return result;
    }
    
}
