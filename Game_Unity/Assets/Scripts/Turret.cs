﻿using UnityEngine;

public class Turret : Unit {
    public bool CanRotate = true;
    public float m_rotationSpeed;
    public float m_rotationArc;
    public float m_fireArc;
    public Transform m_barrel;
    public float m_initialRot;
    public Vector3 m_initialForward;
    public Weapon m_weapon;

    public void SetData(TurretData turretData)
    {
        m_fireArc = turretData.FireArcInDegrees;
        m_startupTime = turretData.StartupTime;
        m_rotationSpeed = turretData.RotationSpeedInDegreesPerSecond;
        m_rotationArc = turretData.RotationArcInDegrees;
        m_fireArc = turretData.FireArcInDegrees;
        m_startupTime = turretData.StartupTime;

        Reward = (int)(turretData.Cost * GameManager.Instance.m_attackRewardScalar);

        // To offset other unit fire times
        m_timePassed = Random.Range(0.0f, 3f);

        m_initialRot = transform.localEulerAngles.z;
        m_initialRot = TransformAngle(m_initialRot);
        m_initialForward = transform.worldToLocalMatrix * transform.up;

        SetHealth(turretData.Health);
    }

    // Use this for initialization
    void Start()
    {
        Init();

        Health = GetComponent<Health>();
        Health?.FullHeal();

        Health.DiedEvent += OnDeath;

        m_weapon.PopulateFromStaticData();
	}

    void Update()
    {
        // Enemy behaviour
        if (!IsEnemy)
            return;

        m_timePassed += Time.deltaTime;
        if (m_timePassed < m_startupTime)
        {
            if (m_sprites != null)
            {
                foreach (var sprite in m_sprites)
                {
                    var color = sprite.color;
                    color.a = m_timePassed / m_startupTime;
                    sprite.color = color;
                }
            }
            return;
        }

        Vector3 targetVector = (m_player.position - transform.position).normalized;

        if (CanRotate)
        {
            // Calculate and rotate the target vector so that Y is the aim axis for the turrets. This requires a 90f rotatation;
            float spreadAngle = Mathf.Sin(m_timePassed * 1.2f) * m_fireArc;
            var rotationTarget =  Quaternion.LookRotation(targetVector, -Vector3.forward) * Quaternion.Euler(90f, 0f, 0f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation,
                Quaternion.Euler(0f, 0f, rotationTarget.eulerAngles.z + spreadAngle),
                m_rotationSpeed * Time.deltaTime);
        }

        m_weapon.Fire(m_player);
    }

    private float TransformAngle(float angle)
    {
        while (angle < 0.0f)
        {
            angle += 360.0f;
        }
        while (angle > 360.0f)
        {
            angle -= 360.0f;
        }

        return angle;
    }
}
