using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipGenerator : MonoBehaviour
{
    public float CountDownInSec;
    private float m_elapsedTime;

    private void Awake()
    {
        GameManager.Instance.PlayerShipGenerator = this;
    }

    public void RegenerateShip(float countdown)
    {
        CountDownInSec = countdown;
        m_elapsedTime = 0;
        StopAllCoroutines();
        StartCoroutine(RegenerateShipRoutine());
    }

    private IEnumerator RegenerateShipRoutine()
    {
        while(m_elapsedTime < CountDownInSec)
        {
            m_elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            GameManager.Instance.GameUI.UpdateShipRegenCountdown((int)(CountDownInSec - m_elapsedTime));
        }

        GameManager.Instance.CreatePlayer();
    }

    public void CancelRegen()
    {
        m_elapsedTime = 0;
        StopAllCoroutines();
    }

}
