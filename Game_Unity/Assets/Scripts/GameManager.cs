﻿using System.Collections;
using Toolset.Core;
using UnityEngine;
using Toolset.ProtocolBuffers;
using System.Collections.Generic;

public class GameManager : SingletonBehaviour<GameManager>
{
    // Editable properties
    //
    [Header("Editable Properties")]
    public float m_attackRewardScalar = 0.5f;
    public float m_goldRegenerationRateBySec = 1;
    public float m_playerShipRepairInSec = 10;
    public float m_saveAfterSec = 30f;

    // 
    // Ship Data KeysrewardText.text = GameManager.Instance.m_re
    //
    public string[] m_weaponTiers;
    [HideInInspector]
    public int m_weaponTiersIndex = 0;

    public string[] m_shieldTiers;
    [HideInInspector]
    public int m_shieldTiersIndex = 0;

    public string[] m_speedTiers;
    [HideInInspector]
    public int m_speedTiersIndex = 0;

    public string[] m_shipTiers;
    [HideInInspector]
    public int m_shipTiersIndex = 0;


    //
    // Planet Data Keys
    //
    public string[] m_planetTiers;
    [HideInInspector]
    public int m_planetTiersIndex = 0;

    public string[] m_turretTiers;
    [HideInInspector]
    public int m_turretTiersIndex = 0;

    public string[] m_hangerTiers;
    [HideInInspector]
    public int m_hangerTiersIndex = 0;

    public string[] m_fighterTiers;
    [HideInInspector]
    public int m_fighterTiersIndex = 0;

    public string[] m_planetShieldTiers;
    [HideInInspector]
    public int m_planetShieldTiersIndex = 0;

    public string[] m_fleetTiers;
    [HideInInspector]
    public int m_fleetTiersIndex = 0;


    // Un editable properties
    //
    [Header("Uneditable Properties")]
    public float m_delayedSaveInSec = 30;
    private Coroutine m_delayedSaveCoroutine;

    [HideInInspector]
    public PlayerController PlayerController;
    [HideInInspector]
    public ObjectPoolManager ObjectPoolManager;
    [HideInInspector]
    public Planet HomePlanet;
    [HideInInspector]
    public Planet EnemyPlanet;
    [HideInInspector]
    public Shop Shop;
    [HideInInspector]
    public PlayerShip PlayerShip;
    [HideInInspector]
    public PlayerCamera PlayerCamera;
    [HideInInspector]
    public PlayerShipGenerator PlayerShipGenerator;
    [HideInInspector]
    public GameUI GameUI;
    [HideInInspector]
    public MainMenu MainMenu;
    [HideInInspector]
    public GoldGenerator GoldGenerator;
    [HideInInspector]
    public Network Network;

    public GameObject PlayerShipPrefab;

    // UI
    //
    [HideInInspector]
    public GameObject m_shopCanvas;

    // Save File
    private const string c_localPlanetSaveFile = "LocalPlanet";
    private const string c_localPlayerSaveFile = "LocalPlayer";
    private float m_saveElapsedTime;

    //
    // Data
    //
    public PlanetData PlayerPlanetData;
    public PlanetData EnemyPlanetData;
    public PlayerData PlayerData;
    public List<PlanetData> Neighbors = new List<PlanetData>();

    //
    // Invasion
    //
    public int RewardTally;

    public enum GameState
    {
        MainMenu,
        AtHomePlanet,
        PlayerRegen,
        InvadingEnemyPlanet,
        DestroyedEnemyPlanet,
        Count
    }

    private GameState m_state;


    //
    // Properties
    //
    public GameState State => m_state;
    public bool IsAtHome => State == GameState.AtHomePlanet || State == GameState.PlayerRegen;
    public bool IsPlayerDead => PlayerShip.IsDead;

    //
    // Functions
    //

    private void Awake()
    {
        GameUI = FindObjectOfType<GameUI>(includeInactive:true);

        Shop = FindObjectOfType<Shop>(includeInactive:true);
        m_shopCanvas = Shop.transform.parent.gameObject;

        var planets = FindObjectsOfType<Planet>(includeInactive:true);
        foreach(var planet in planets)
        {
            if (planet.IsHomePlanet)
                HomePlanet = planet;
            else
                EnemyPlanet = planet;
        }
    }

    public Planet FindHomePlanet()
    {
        var planets = FindObjectsOfType<Planet>(includeInactive:true);
        foreach(var planet in planets)
        {
            if (planet.IsHomePlanet)
                return planet;
        }
        return null;
    }

    // Use this for initialization
    void Start()
    {
        m_state = GameState.MainMenu;
        PostStateChanged();

        //HomePlanet.PlanetWater.UpdatedHandler += OnPlayerPlanetWaterUpdated;
        //ContinueGame();
        //GetOtherPlanet();
    }

    public void CreatePlayer()
    {
        // If the player is being regen, cancel request
        PlayerShipGenerator.CancelRegen();

        GameObject go = ObjectPoolManager.Spawn(PlayerShipPrefab, HomePlanet.transform.position - new Vector3(0, 0, 1), HomePlanet.transform.rotation);
        PlayerShip = go.GetComponent<PlayerShip>();
        PlayerController = go.GetComponent<PlayerController>();
        PlayerController.CanMove = !IsAtHome;
        PlayerController.ParkHome();
        PlayerCamera.Init(go.transform);

        m_state = GameState.AtHomePlanet;
        PostStateChanged();
    }

    private void OnEnable()
    {
        EventManager.AddListener<EnemyDestroyed>(OnEnemyDestroyed);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<EnemyDestroyed>(OnEnemyDestroyed);
    }

    public void PostStateChanged()
    {
        switch(m_state)
        {
            case GameState.MainMenu:
                MainMenu.gameObject.SetActive(true);
                m_shopCanvas.SetActive(false);
                HomePlanet.gameObject.SetActive(true);
                EnemyPlanet.gameObject.SetActive(false);
                GameUI.gameObject.SetActive(false);
                break;
            case GameState.AtHomePlanet:
                MainMenu.gameObject.SetActive(false);
                m_shopCanvas.SetActive(true);
                HomePlanet.gameObject.SetActive(true);
                EnemyPlanet.gameObject.SetActive(false);
                PlayerController.CanMove = false;
                PlayerController.ParkHome();
                GameUI.gameObject.SetActive(true);

                Network.GetNeighbors();
                break;
            case GameState.PlayerRegen:
                MainMenu.gameObject.SetActive(false);
                m_shopCanvas.SetActive(true);
                HomePlanet.gameObject.SetActive(true);
                EnemyPlanet.gameObject.SetActive(false);
                PlayerController.CanMove = false;
                PlayerController.ParkHome();
                GameUI.gameObject.SetActive(true);

                PlayerShipGenerator.RegenerateShip(HomePlanet.PlanetData.ShipRepairCost);
                break;
            case GameState.InvadingEnemyPlanet:
                MainMenu.gameObject.SetActive(false);
                m_shopCanvas.SetActive(false);
                HomePlanet.gameObject.SetActive(false);
                EnemyPlanet.gameObject.SetActive(true);
                PlayerController.CanMove = true;

                // TODO : Show Game UI until we have the code to show only when all enemies are destroyed
                GameUI.gameObject.SetActive(true);
                break;
        }

        EventManager.Fire(new GameStateChanged(State));
    }

    public void NewGame(string planetName)
    {
        CreatePlayer();
        m_shopCanvas.SetActive(true);

        // Planet
        //
        m_planetTiersIndex = 0;
        PlayerPlanetData = new PlanetData();
        PlayerPlanetData.PopulateFrom(StaticData.GetPlanetData(m_planetTiers[m_planetTiersIndex]));

        PlayerPlanetData.xPlanetName = planetName;
        PlayerPlanetData.xGoldCount = PlayerPlanetData.GoldMax;

        m_turretTiersIndex = 0;
        PlayerPlanetData.xTurretLevel = m_turretTiersIndex;
        PlayerPlanetData.xTurretId = m_turretTiers[m_turretTiersIndex];

        m_hangerTiersIndex = 0;
        PlayerPlanetData.xHangerLevel = m_hangerTiersIndex;
        PlayerPlanetData.xHangerId = m_hangerTiers[m_hangerTiersIndex];

        m_fighterTiersIndex = 0;
        PlayerPlanetData.xFighterLevel = m_fighterTiersIndex;
        PlayerPlanetData.xFighterId = m_fighterTiers[m_fighterTiersIndex];

        m_planetShieldTiersIndex = 0;
        PlayerPlanetData.xShieldLevel = m_planetShieldTiersIndex;
        PlayerPlanetData.xShieldId = m_planetShieldTiers[m_planetShieldTiersIndex];
        
        m_fleetTiersIndex = 0;
        PlayerPlanetData.xFleetLevel = m_fleetTiersIndex;
        PlayerPlanetData.xFleetId = m_fleetTiers[m_fleetTiersIndex];

        HomePlanet.SetData(PlayerPlanetData);
        EventManager.Fire(new PlanetDataUpdated(PlayerPlanetData));

        // Ship

        PlayerData = new PlayerData();
        PlayerData.PopulateFrom(StaticData.GetPlayerData());

        m_weaponTiersIndex = 0;
        PlayerData.xWeaponLevel = m_weaponTiersIndex;
        PlayerData.WeaponDataId = m_weaponTiers[m_weaponTiersIndex];

        m_shieldTiersIndex = 0;
        PlayerData.xShieldLevel = m_shieldTiersIndex;
        PlayerData.ShieldDataId = m_shieldTiers[m_shieldTiersIndex];

        m_speedTiersIndex = 0;
        PlayerData.xSpeedLevel = m_speedTiersIndex;
        PlayerData.SpeedDataId = m_speedTiers[m_speedTiersIndex];

        m_shipTiersIndex = 0;
        PlayerData.xShipLevel = m_shipTiersIndex;
        PlayerData.ShipDataId = m_shipTiers[m_shipTiersIndex];

        PlayerShip.SetData(PlayerData);

        EventManager.Fire(new PlayerDataUpdated(PlayerData));

        m_state = GameState.AtHomePlanet;
        PostStateChanged();

        GoldGenerator.StartGoldGenerator();
    }
    
    public IEnumerator ContinueGameRoutine(string planetName)
    {
        PlayerPlanetData = null;

        int attempts = 0;
        while (PlayerPlanetData == null)
        {
            yield return Network.LoadDataRoutine(planetName);
            if (attempts++ >= 3)
                break;
        }

        if (PlayerPlanetData == null)
        {
            Debug.Log("--- Could not retreive planet data. Creating a new one");
            PlayerPlanetData = new PlanetData();
            PlayerPlanetData.PopulateFrom(StaticData.GetPlanetData(m_planetTiers[m_planetTiersIndex]));

            PlayerPlanetData.xPlanetName = planetName;
            PlayerPlanetData.xGoldCount = PlayerPlanetData.GoldMax;
        }

        CreatePlayer();

        m_turretTiersIndex = PlayerPlanetData.xTurretLevel;
        m_hangerTiersIndex = PlayerPlanetData.xHangerLevel;
        m_fighterTiersIndex = PlayerPlanetData.xFighterLevel;
        m_planetShieldTiersIndex = PlayerPlanetData.xShieldLevel;
        m_fleetTiersIndex = PlayerPlanetData.xFleetLevel;
        m_planetTiersIndex = PlayerPlanetData.xPlanetLevel;

        PlayerPlanetData.xTurretId = m_turretTiers[m_turretTiersIndex];
        PlayerPlanetData.xHangerId = m_hangerTiers[m_hangerTiersIndex];
        PlayerPlanetData.xFighterId = m_fighterTiers[m_fighterTiersIndex];
        PlayerPlanetData.xShieldId = m_planetShieldTiers[m_planetShieldTiersIndex];
        PlayerPlanetData.xFleetId = m_fleetTiers[m_fleetTiersIndex];

        HomePlanet.SetData(PlayerPlanetData);
        EventManager.Fire(new PlanetDataUpdated(PlayerPlanetData));

        if (PlayerData == null)
        {
            PlayerData = new PlayerData();
            PlayerData.PopulateFrom(StaticData.GetPlayerData());
        }

        m_weaponTiersIndex = PlayerData.xWeaponLevel;
        m_shieldTiersIndex = PlayerData.xShieldLevel;
        m_speedTiersIndex = PlayerData.xSpeedLevel;
        m_shipTiersIndex = PlayerData.xShipLevel;

        PlayerData.WeaponDataId = m_weaponTiers[m_weaponTiersIndex];
        PlayerData.ShieldDataId = m_shieldTiers[m_shieldTiersIndex];
        PlayerData.SpeedDataId = m_speedTiers[m_speedTiersIndex];
        PlayerData.ShipDataId = m_shipTiers[m_shipTiersIndex];

        PlayerShip.SetData(PlayerData);
        EventManager.Fire(new PlayerDataUpdated(PlayerData));

        string message = $"Obtained HOME PlanetData : {PlayerPlanetData.PlanetDataId}\n";
        message += $"Planet Name : {PlayerPlanetData.xPlanetName}\n";
        message += $"Upgrade Cost : {PlayerPlanetData.UpgradeCost}\n";
        message += $"Gold Count: {PlayerPlanetData.xGoldCount}\n";
        message += $"Turrets : {PlayerPlanetData.xTurretCount}\n";
        message += $"Turret Lvl : {PlayerPlanetData.xTurretLevel}\n";
        message += $"Fighters : {PlayerPlanetData.xFighterCount}\n";
        message += $"Fighters Lvl : {PlayerPlanetData.xFighterLevel}\n";
        message += $"Hangers : {PlayerPlanetData.xHangerCount}\n";
        message += $"Hangers Lvl : {PlayerPlanetData.xHangerLevel}\n";
        message += $"Shields : {PlayerPlanetData.xShieldCount}\n";
        message += $"Shields Lvl : {PlayerPlanetData.xShieldLevel}\n";
        message += $"Fleet : {PlayerPlanetData.xFleetCount}\n";
        message += $"Fleet Lvl : {PlayerPlanetData.xFleetLevel}\n";
        message += $"Planet Lvl : {PlayerPlanetData.xPlanetLevel}\n";
        message += $"Ship Weapon : {PlayerData.xWeaponLevel}\n";
        message += $"Ship Shields : {PlayerData.xShieldLevel}\n";
        message += $"Ship Speed : {PlayerData.xSpeedLevel}\n";
        message += $"Ship lvl : {PlayerData.xShipLevel}\n";
        Debug.Log(message);


        m_state = GameState.AtHomePlanet;
        PostStateChanged();

        GoldGenerator.StartGoldGenerator();
    }

    public void ContinueGame(string planetName)
    {
        StartCoroutine(ContinueGameRoutine(planetName));
    }

    public void DelayedSave()
    {
        if (m_delayedSaveCoroutine != null)
            StopCoroutine(m_delayedSaveCoroutine);

        m_delayedSaveCoroutine = StartCoroutine(DelayedSaveRoutine(m_delayedSaveInSec));
    }

    public IEnumerator DelayedSaveRoutine(float delayedSaveInSec)
    {
        yield return new WaitForSeconds(delayedSaveInSec);
        ForceSave();
        m_delayedSaveCoroutine = null;
    }

    public void ForceSave()
    {
        if (PlayerController == null)
            return;

        if (m_delayedSaveCoroutine != null)
            StopCoroutine(m_delayedSaveCoroutine);

        PlayerPlanetData.PlanetDataId = PlayerData.PlayerDataId;
        PlayerPlanetData.xGoldCount = HomePlanet.PlanetWater.Amount;
        SaveManager.SaveModelInstance(c_localPlanetSaveFile, PlayerPlanetData);
        SaveManager.SaveModelInstance(c_localPlayerSaveFile, PlayerData);

        // Only upload if planet has defenses
        if (PlayerPlanetData.xTurretCount + PlayerPlanetData.xFighterCount + PlayerPlanetData.xHangerCount + PlayerPlanetData.xFleetCount >= 2)
            Network.SaveData();
    }

    private IEnumerator UploadPlanetData()
    {
        UploadPlanetDataRequest uploadPlanetDataRequest = new UploadPlanetDataRequest(PlayerPlanetData);
        yield return uploadPlanetDataRequest.Send();
    }

    public void GetOtherPlanet()
    {
        if (PlayerController == null)
            return;
        
        RoutineManager.I.StartRoutine(DownloadPlanetData());
    }
    
    private IEnumerator DownloadPlanetData()
    {
        Debug.LogError("Deprecated DownloadPlanetData");
        yield return null;

        //DownloadPlanetDataRequest downloadPlanetDataRequest = new DownloadPlanetDataRequest(PlayerData.PlayerDataId);
        //yield return downloadPlanetDataRequest.Send();

        //EnemyPlanetData = downloadPlanetDataRequest.ResponseData; // This is a random enemy player planet data obtained from the server

        ////if (EnemyPlanetData.PlanetDataId == "00840734-ab48-4bb6-91e5-1a7d8f470c3b")
        ////    Debug.Log("Obtained Planet with ID {0} from Server!".StringBuilderFormat(EnemyPlanetData.PlanetDataId));

        //string message = $"Obtained ENEMY PlanetData : {EnemyPlanetData.PlanetDataId}\n";
        //message += $"Planet Name : {EnemyPlanetData.xPlanetName}\n";
        //message += $"Upgrade Cost : {EnemyPlanetData.UpgradeCost}\n";
        //message += $"Turrets : {EnemyPlanetData.xTurretCount}\n";
        //message += $"Fighters : {EnemyPlanetData.xFighterCount}\n";
        //message += $"Hangers : {EnemyPlanetData.xHangerCount}\n";
        //message += $"Shields : {EnemyPlanetData.xShieldCount}\n";
        //message += $"Fleet : {EnemyPlanetData.xFleetCount}\n";
        //Debug.Log(message);

        //// TODO : Remove hacks when this is fixed.
        //EnemyPlanetData.UpgradeCost = HomePlanet.PlanetData.UpgradeCost;
    }

    public void GoHome()
    {
        // Tear down planet
        EnemyPlanet.TearDownPlanet();

        // Regen player if dead
        if (PlayerShip.IsDead)
        {
            m_state = GameState.PlayerRegen;
        }
        else
        {
            m_state = GameState.AtHomePlanet;
        }

        PostStateChanged();

        // Init Home Planet
        HomePlanet.SetData();

        // Add any rewards
        HomePlanet.AddGold(RewardTally);
        RewardTally = 0;
    }

    public void AttackSelf()
    {
        ForceSave();

        m_state = GameState.InvadingEnemyPlanet;
        HomePlanet.TearDownPlanet();

        PostStateChanged();

        PlanetData enemyPlanetData = SaveManager.LoadModelInstance<PlanetData>(c_localPlanetSaveFile);
        EnemyPlanet.SetData(enemyPlanetData);
        PlayerController.SetupInvade(enemyPlanetData);

        RewardTally = 0;

        PostStateChanged();
    }

    public void InvadeEnemyPlanet()
    {
        if (PlayerController == null)
            return;

        ForceSave();

        m_state = GameState.InvadingEnemyPlanet;

        RoutineManager.I.StartRoutine(StartNewEnemyPlanetInvasion());
    }

    public void ClearEnemyPlanetData()
    {
        EnemyPlanetData = null;
    }

    private IEnumerator StartNewEnemyPlanetInvasion()
    {
        if (PlayerController == null)
            yield break;

        Neighbors.Clear();

        int attempts = 0;
        while (Neighbors.Count == 0)
        {
            yield return Network.GetNeighborsRoutine();
            Debug.Log($"Attempting get neighbors to invade. Attempt {attempts}");
            if (attempts++ >= 3)
                break;
        }

        HomePlanet.TearDownPlanet();
        PostStateChanged();
        RewardTally = 0;

        // If couldn't get neighbors attack self
        if (Neighbors.Count == 0)
        {
            Debug.Log("Could not get enemy planet data, attacking self");
            GameManager.Instance.AttackSelf();
        }
        else
        {
            EnemyPlanetData = Neighbors[Random.Range(0, Neighbors.Count)];
            EnemyPlanetData.xTurretId = m_turretTiers[EnemyPlanetData.xTurretLevel];
            EnemyPlanetData.xHangerId = m_hangerTiers[EnemyPlanetData.xHangerLevel];
            EnemyPlanetData.xFighterId = m_fighterTiers[EnemyPlanetData.xFighterLevel];
            EnemyPlanetData.xShieldId = m_planetShieldTiers[EnemyPlanetData.xShieldLevel];
            EnemyPlanetData.xFleetId = m_fleetTiers[EnemyPlanetData.xFleetLevel];

            Debug.Log($"Attacking planet {EnemyPlanetData.xPlanetName}");
            EnemyPlanet.SetData(EnemyPlanetData);
            PlayerController.SetupInvade(EnemyPlanetData);
        }
    }

    public void OnPlayerPlanetWaterUpdated(object sender, System.EventArgs e)
    {
        if (m_state != GameState.AtHomePlanet)
            return;

        HomePlanet.PlanetData.xGoldCount = HomePlanet.PlanetWater.Amount;
    }

    public void UpgradePlanet()
    {
        if (m_planetTiersIndex >= m_planetTiers.Length)
            return;

        m_planetTiersIndex++;
        PlayerPlanetData.PopulateFrom(StaticData.GetPlanetData(m_planetTiers[m_planetTiersIndex]));
        PlayerPlanetData.xPlanetLevel = m_planetTiersIndex;
        HomePlanet.SetData(PlayerPlanetData);
        EventManager.Fire(new PlanetDataUpdated(PlayerPlanetData));

        DelayedSave();
    }

    public void UpgradeTurret()
    {
        if (m_turretTiersIndex >= m_turretTiers.Length)
            return;

        HomePlanet.RemoveGold(HomePlanet.TurretData.UpgradeCost);

        m_turretTiersIndex++;
        string id = m_turretTiers[m_turretTiersIndex];
        TurretData data = StaticData.GetTurretData(id);
        HomePlanet.PlanetData.xTurretId = id;
        HomePlanet.PlanetData.xTurretLevel = m_turretTiersIndex;
        HomePlanet.SetTurretData(data);
        DelayedSave();
    }

    public void UpgradeHanger()
    {
        if (m_hangerTiersIndex >= m_hangerTiers.Length)
            return;

        HomePlanet.RemoveGold(HomePlanet.HangerData.UpgradeCost);

        m_hangerTiersIndex++;
        string id = m_hangerTiers[m_hangerTiersIndex];
        HangerData data = StaticData.GetHangerData(id);
        HomePlanet.PlanetData.xHangerId = id;
        HomePlanet.PlanetData.xHangerLevel = m_hangerTiersIndex;
        HomePlanet.SetHangerData(data);
        DelayedSave();
    }

    public void UpgradeFighter()
    {
        if (m_fighterTiersIndex >= m_fighterTiers.Length)
            return;

        HomePlanet.RemoveGold(HomePlanet.FighterData.UpgradeCost);

        m_fighterTiersIndex++;
        string id = m_fighterTiers[m_fighterTiersIndex];
        FighterData data = StaticData.GetFighterData(id);
        HomePlanet.PlanetData.xFighterId = id;
        HomePlanet.PlanetData.xFighterLevel = m_fighterTiersIndex;
        HomePlanet.SetFighterData(data);
        DelayedSave();
    }

    public void UpgradePlanetShield()
    {
        if (m_planetShieldTiersIndex >= m_planetShieldTiers.Length)
            return;

        HomePlanet.RemoveGold(HomePlanet.PlanetShieldData.UpgradeCost);

        m_planetShieldTiersIndex++;
        string id = m_planetShieldTiers[m_planetShieldTiersIndex];
        PlanetShieldData data = StaticData.GetPlanetShieldData(id);
        HomePlanet.PlanetData.xShieldId = id;
        HomePlanet.PlanetData.xShieldLevel = m_planetShieldTiersIndex;
        HomePlanet.SetPlanetShieldData(data);
        DelayedSave();
    }

    public void UpgradeFleet()
    {
        if (m_fleetTiersIndex >= m_fleetTiers.Length)
            return;

        HomePlanet.RemoveGold(HomePlanet.FleetData.UpgradeCost);

        m_fleetTiersIndex++;
        string id = m_fleetTiers[m_fleetTiersIndex];
        FleetData data = StaticData.GetFleetData(id);
        HomePlanet.PlanetData.xFleetId = id;
        HomePlanet.PlanetData.xFleetLevel = m_hangerTiersIndex;
        HomePlanet.SetFleetData(data);
        DelayedSave();
    }

    public void UpgradeWeapon()
    {
        if (m_weaponTiersIndex >= m_weaponTiers.Length)
            return;

        HomePlanet.RemoveGold(PlayerShip.WeaponData.UpgradeCost);

        m_weaponTiersIndex++;
        string id = m_weaponTiers[m_weaponTiersIndex];
        PlayerData.WeaponDataId = id;
        PlayerData.xWeaponLevel = m_weaponTiersIndex;

        PlayerShip.UpgradeWeapon(m_weaponTiersIndex, id);

        Shop.RefreshShipUpgradeCosts();
        DelayedSave();
    }

    public void UpgradeShield()
    {
        if (m_shieldTiersIndex >= m_shieldTiers.Length)
            return;

        HomePlanet.RemoveGold(PlayerShip.ShieldData.UpgradeCost);

        m_shieldTiersIndex++;
        string id = m_shieldTiers[m_shieldTiersIndex];
        PlayerData.ShieldDataId = id;
        PlayerData.xShieldLevel = m_shieldTiersIndex;

        PlayerShip.UpgradeShield(m_shieldTiersIndex, id);

        Shop.RefreshShipUpgradeCosts();
        DelayedSave();
    }

    public void UpgradeSpeed()
    {
        if (m_speedTiersIndex >= m_speedTiers.Length)
            return;

        HomePlanet.RemoveGold(PlayerShip.SpeedData.UpgradeCost);

        m_speedTiersIndex++;
        string id = m_speedTiers[m_speedTiersIndex];
        PlayerData.SpeedDataId = id;
        PlayerData.xSpeedLevel = m_speedTiersIndex;

        PlayerShip.UpgradeSpeed(m_speedTiersIndex, id);

        Shop.RefreshShipUpgradeCosts();
        DelayedSave();
    }

    public void UpgradeShip()
    {
        if (m_shipTiersIndex >= m_shipTiers.Length)
            return;

        HomePlanet.RemoveGold(PlayerShip.ShipData.UpgradeCost);

        m_shipTiersIndex++;
        string id = m_shipTiers[m_shipTiersIndex];
        PlayerData.ShipDataId = id;
        PlayerData.xShipLevel = m_shipTiersIndex;

        PlayerShip.UpgradeShip(m_shipTiersIndex, id);

        Shop.RefreshShipUpgradeCosts();
        DelayedSave();

    }

    private void OnEnemyDestroyed(EnemyDestroyed evt)
    {
        // Add to tally. Player must survive to collect loot
       RewardTally += evt.Reward;

        // Check if planet is dead
        if (EnemyPlanet.IsDead)
        {
            m_state = GameState.DestroyedEnemyPlanet;
            EventManager.Fire(new GameStateChanged(m_state));
        }    
    }

    public void FailedInvasion()
    {
        // Remove accumulated rewards
        RewardTally = 0;

        // Go home
        GoHome();
    }


    private void OnApplicationQuit()
    {
        ForceSave();
    }
}
