﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    private Planet m_homePlanet;
    private Planet HomePlanet
    {
        get
        {
            if (m_homePlanet == null)
                return GameManager.Instance.FindHomePlanet();
            return m_homePlanet;
        }
        set
        {
            m_homePlanet = value;
        }
    }

    [Header("Ship")]
    public GameObject m_playerPrefab;
    public Color m_playerColor;
    public int m_maxUpgradeCost = 5;
    public int m_ship = 1;

    [Header("Weapon")]
    public Button m_weaponButton;
    public Text m_weaponUpgradeCountText;
    public Text m_weaponUpgradeCostText;

    public int m_weaponIndex = 0;
    public GameObject[] m_weaponUpgrades;

    [Header("Ship")]
    public Button m_shipButton;
    public Text m_shipUpgradeCountText;
    public Text m_shipUpgradeCostText;

    public int m_shipIndex = 0;
    public GameObject[] m_shipUpgrades;

    [Header("Shields")]
    public Button m_shieldButton;
    public Text m_shieldUpgradeCountText;
    public Text m_shieldUpgradeCostText;

    public int m_shieldIndex = 0;
    public GameObject[] m_shieldUpgrades;

    [Header("Speed")]
    public Button m_speedButton;
    public Text m_speedUpgradeCountText;
    public Text m_speedUpgradeCostText;

    public int m_speedIndex = 0;
    public GameObject[] m_speedUpgrades;


    [Header("Tank")]
    public Button m_waterTankButton;
    public int m_waterTankCost = 1;
    public int m_waterTankIncrease = 1;
    public int m_waterTankIndex = 0;
    public GameObject[] m_waterTankUpgrades;

    //
    // Shop Vars
    //
    private PlayerShip m_playerShip;
    private GameManager m_gameManager;

    //
    // Shop UI
    //
    [Header("UI")]
    public Text m_goldAmountText;
    private int m_originalRewardFontSize;
    private Coroutine m_rewardCoroutine;
    private int m_lastGoldAmount;

    public Image[] m_waterTankCostDrops;

    [Header("Attack")]
    public Button m_attackButton;

    [Header("Turret")]
    public Button m_turretBuyButton;
    public Button m_turretUpgradeButton;
    public Text m_turretCountText;
    public Text m_turretCostText;
    public Text m_turretUpgradeCountText;
    public Text m_turretUpgradeCostText;

    [Header("Hanger")]
    public Button m_hangerBuyButton;
    public Button m_hangerUpgradeButton;
    public Text m_hangerCountText;
    public Text m_hangerCostText;
    public Text m_hangerUpgradeCountText;
    public Text m_hangerUpgradeCostText;

    [Header("Fighter")]
    public Button m_fighterBuyButton;
    public Button m_fighterUpgradeButton;
    public Text m_fighterCountText;
    public Text m_fighterCostText;
    public Text m_fighterUpgradeCountText;
    public Text m_fighterUpgradeCostText;

    [Header("Planet Shield")]
    public Button m_planetShieldBuyButton;
    public Button m_planetShieldUpgradeButton;
    public Text m_planetShieldCountText;
    public Text m_planetShieldCostText;
    public Text m_planetShieldUpgradeCountText;
    public Text m_planetShieldUpgradeCostText;

    [Header("Fleet")]
    public Button m_fleetBuyButton;
    public Button m_fleetUpgradeButton;
    public Text m_fleetCountText;
    public Text m_fleetCostText;
    public Text m_fleetUpgradeCountText;
    public Text m_fleetUpgradeCostText;

    [Header("Planet Upgrade")]
    public Button m_planetUpgradeButton;
    public Text m_planetUpgradeCountText;
    public Text m_planetUpgradeCostText;
    
    //
    // References to data
    //
    public TurretData TurretData { get; private set; }
    public HangerData HangerData { get; private set; }
    public PlanetShieldData PlanetShieldData { get; private set; }
    public FleetData FleetData { get; private set; }
    public FighterData FighterData { get; private set; }

    public WeaponData WeaponData { get; private set; }
    public ShieldData ShieldData { get; private set; }
    public SpeedData SpeedData { get; private set; }
    public ShipData ShipData { get; private set; }


    // Use this for initialization
    void Start ()
    {
        m_gameManager = GameManager.Instance;
        HomePlanet = m_gameManager.HomePlanet;
        m_gameManager.Shop = this;

        m_originalRewardFontSize = m_goldAmountText.fontSize;

        if(m_weaponButton != null)
            m_weaponButton.onClick.AddListener(delegate () { UpgradeWeapon();});

        if (m_shieldButton != null)
            m_shieldButton.onClick.AddListener(delegate () { UpgradeShield();});

        if(m_speedButton != null)
            m_speedButton.onClick.AddListener(delegate () { UpgradeSpeed();});

        if (m_shipButton != null)
            m_shipButton.onClick.AddListener(delegate () { UpgradeShip(); });

        //if (m_waterTankButton != null)
        //    m_waterTankButton.onClick.AddListener(delegate () { UpgradeWaterTank(); });

        if (m_turretBuyButton != null)
            m_turretBuyButton.onClick.AddListener(delegate () { BuyTurret(); });
        if (m_turretUpgradeButton != null)
            m_turretUpgradeButton.onClick.AddListener(delegate () { UpgradeTurret(); });

        if (m_hangerBuyButton != null)
            m_hangerBuyButton.onClick.AddListener(delegate () { BuyHanger(); });
        if (m_hangerUpgradeButton != null)
            m_hangerUpgradeButton.onClick.AddListener(delegate () { UpgradeHanger(); });

        if (m_fighterBuyButton != null)
            m_fighterBuyButton.onClick.AddListener(delegate () { BuyFighter(); });
        if (m_fighterUpgradeButton != null)
            m_fighterUpgradeButton.onClick.AddListener(delegate () { UpgradeFighter(); });

        if (m_planetShieldBuyButton != null)
            m_planetShieldBuyButton.onClick.AddListener(delegate () { BuyPlanetShield(); });
        if (m_planetShieldUpgradeButton != null)
            m_planetShieldUpgradeButton.onClick.AddListener(delegate () { UpgradePlanetShield(); });

        if (m_fleetBuyButton != null)
            m_fleetBuyButton.onClick.AddListener(delegate () { BuyFleet(); });
        if (m_fleetUpgradeButton != null)
            m_fleetUpgradeButton.onClick.AddListener(delegate () { UpgradeFleet(); });

        if (m_planetUpgradeButton != null)
            m_planetUpgradeButton.onClick.AddListener(delegate () { UpgradePlanet(); });

        if (m_attackButton != null)
            m_attackButton.onClick.AddListener(delegate () { Attack(); });

        //m_playerShip = m_gameManager.PlayerController.GetComponent<PlayerShip>();
        //m_playerColor = m_playerShip.m_shipColor;
    }

    private void OnEnable()
    {
        EventManager.AddListener<PlanetGoldUpdated>(OnGoldUpdated);
        EventManager.AddListener<PlayerDataUpdated>(OnPlayerDataUpdated);
        EventManager.AddListener<PlanetDataUpdated>(OnPlanetDataUpdated);

        EventManager.AddListener<TurretDataUpdated>(OnTurretDataUpdated);
        EventManager.AddListener<HangerDataUpdated>(OnHangerDataUpdated);
        EventManager.AddListener<PlanetShieldDataUpdated>(OnPlanetShieldDataUpdated);
        EventManager.AddListener<FleetDataUpdated>(OnFleetDataUpdated);
        EventManager.AddListener<FighterDataUpdated>(OnFighterDataUpdated);

        EventManager.AddListener<WeaponDataUpdated>(OnWeaponDataUpdated);
        EventManager.AddListener<ShipDataUpdated>(OnShipDataUpdated);
        EventManager.AddListener<SpeedDataUpdated>(OnSpeedDataUpdated);
        EventManager.AddListener<ShieldDataUpdated>(OnShieldDataUpdated);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<PlanetGoldUpdated>(OnGoldUpdated);
        EventManager.RemoveListener<PlayerDataUpdated>(OnPlayerDataUpdated);
        EventManager.RemoveListener<PlanetDataUpdated>(OnPlanetDataUpdated);

        EventManager.RemoveListener<TurretDataUpdated>(OnTurretDataUpdated);
        EventManager.RemoveListener<HangerDataUpdated>(OnHangerDataUpdated);
        EventManager.RemoveListener<PlanetShieldDataUpdated>(OnPlanetShieldDataUpdated);
        EventManager.RemoveListener<FleetDataUpdated>(OnFleetDataUpdated);
        EventManager.RemoveListener<FighterDataUpdated>(OnFighterDataUpdated);

        EventManager.RemoveListener<WeaponDataUpdated>(OnWeaponDataUpdated);
        EventManager.RemoveListener<ShipDataUpdated>(OnShipDataUpdated);
        EventManager.RemoveListener<SpeedDataUpdated>(OnSpeedDataUpdated);
        EventManager.RemoveListener<ShieldDataUpdated>(OnShieldDataUpdated);
    }

    private void OnPlanetDataUpdated(PlanetDataUpdated evt)
    {
        m_planetUpgradeCostText.text = evt.Data.UpgradeCost.ToString();
        m_planetUpgradeCountText.text = $"{GameManager.Instance.m_planetTiersIndex + 1}/{GameManager.Instance.m_planetTiers.Length}";

        RefreshPlanetDefenseSection();
    }

    private void OnPlayerDataUpdated(PlayerDataUpdated evt)
    {
        RefreshPlanetDefenseSection();
        RefreshShipUpgradeCosts();
    }

    private void OnTurretDataUpdated(TurretDataUpdated evt)
    {
        TurretData = evt.Data;
        m_turretCostText.text = TurretData.Cost.ToString();
        m_turretCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xTurretCount}/{TurretData.Max}";
        m_turretUpgradeCostText.text = TurretData.UpgradeCost.ToString();
        m_turretUpgradeCountText.text = $"{GameManager.Instance.m_turretTiersIndex + 1}/{GameManager.Instance.m_turretTiers.Length}";
        RefreshPlanetDefenseSection();
    }

    private void OnHangerDataUpdated(HangerDataUpdated evt)
    {
        HangerData = evt.Data;
        m_hangerCostText.text = HangerData.Cost.ToString();
        m_hangerCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xHangerCount}/{HangerData.Max}";
        m_hangerUpgradeCostText.text = HangerData.UpgradeCost.ToString();
        m_hangerUpgradeCountText.text = $"{GameManager.Instance.m_hangerTiersIndex + 1}/{GameManager.Instance.m_hangerTiers.Length}";
        RefreshPlanetDefenseSection();
    }

    private void OnPlanetShieldDataUpdated(PlanetShieldDataUpdated evt)
    {
        PlanetShieldData = evt.Data;
        m_planetShieldCostText.text = PlanetShieldData.Cost.ToString();
        m_planetShieldCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xShieldCount}/{PlanetShieldData.Max}";
        m_planetShieldUpgradeCostText.text = PlanetShieldData.UpgradeCost.ToString();
        m_planetShieldUpgradeCountText.text = $"{GameManager.Instance.m_planetShieldTiersIndex + 1}/{GameManager.Instance.m_planetShieldTiers.Length}";
        RefreshPlanetDefenseSection();
    }

    private void OnFleetDataUpdated(FleetDataUpdated evt)
    {
        FleetData = evt.Data;
        m_fleetCostText.text = FleetData.Cost.ToString();
        m_fleetCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xFleetCount}/{FleetData.Max}";
        m_fleetUpgradeCostText.text = FleetData.UpgradeCost.ToString();
        m_fleetUpgradeCountText.text = $"{GameManager.Instance.m_fleetTiersIndex + 1}/{GameManager.Instance.m_fleetTiers.Length}";
        RefreshPlanetDefenseSection();
    }

    private void OnFighterDataUpdated(FighterDataUpdated evt)
    {
        FighterData = evt.Data;
        m_fighterCostText.text = FighterData.Cost.ToString();
        m_fighterCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xFighterCount}/{FighterData.Max}";
        m_fighterUpgradeCostText.text = FighterData.UpgradeCost.ToString();
        m_fighterUpgradeCountText.text = $"{GameManager.Instance.m_fighterTiersIndex + 1}/{GameManager.Instance.m_fighterTiers.Length}";
        RefreshPlanetDefenseSection();
    }

    private void OnWeaponDataUpdated(WeaponDataUpdated evt)
    {
        WeaponData = evt.Data;
        RefreshShipUpgradeCosts();
    }

    private void OnShieldDataUpdated(ShieldDataUpdated evt)
    {
        ShieldData = evt.Data;
        RefreshShipUpgradeCosts();
    }

    private void OnShipDataUpdated(ShipDataUpdated evt)
    {
        ShipData = evt.Data;
        RefreshShipUpgradeCosts();
    }

    private void OnSpeedDataUpdated(SpeedDataUpdated evt)
    {
        SpeedData = evt.Data;
        RefreshShipUpgradeCosts();
    }

    private void OnGoldUpdated(PlanetGoldUpdated evt)
    {
        if (m_rewardCoroutine != null)
            StopCoroutine(m_rewardCoroutine);

        m_goldAmountText.text = $"{evt.Amount} / {evt.MaxGold}";

        // TODO : Fix this feedback
        //StartCoroutine(GoldRewardFeedbackRoutine(evt.Amount, evt.MaxGold));

        RefreshPlanetDefenseSection();
        RefreshPlayerShipSection();
    }

    //private IEnumerator GoldRewardFeedbackRoutine(int amount, int maxGold)
    //{
    //    int reward = amount - m_lastGoldAmount;

    //    while (reward > 0)
    //    {
    //        reward--;
    //        m_goldAmountText.text = $"{m_lastGoldAmount++} / {maxGold}";
    //        m_goldAmountText.fontSize = m_originalRewardFontSize + 6;
    //        yield return new WaitForEndOfFrame();
    //        yield return new WaitForEndOfFrame();
    //        m_goldAmountText.fontSize = m_originalRewardFontSize;
    //    }

    //    m_lastGoldAmount = amount;
    //    m_goldAmountText.text = $"{amount} / {maxGold}";
    //}


    public void RefreshPlayerShipSection()
    {
        if (WeaponData != null && (HomePlanet.Gold < WeaponData.UpgradeCost || m_gameManager.m_weaponTiersIndex + 1 >= m_gameManager.m_weaponTiers.Length))
            m_weaponButton.interactable = false;
        else
            m_weaponButton.interactable = true;

        if (ShieldData != null && (HomePlanet.Gold < ShieldData.UpgradeCost || m_gameManager.m_shieldTiersIndex + 1 >= m_gameManager.m_shieldTiers.Length))
            m_shieldButton.interactable = false;
        else
            m_shieldButton.interactable = true;

        if (SpeedData != null && (HomePlanet.Gold < SpeedData.UpgradeCost || m_gameManager.m_speedTiersIndex + 1 >= m_gameManager.m_speedTiers.Length))
            m_speedButton.interactable = false;
        else
            m_speedButton.interactable = true;

        if (ShipData != null && (HomePlanet.Gold < ShipData.UpgradeCost || m_gameManager.m_shipTiersIndex + 1 >= m_gameManager.m_shipTiers.Length))
            m_shipButton.interactable = false;
        else
            m_shipButton.interactable = true;
    }

    public void RefreshShipUpgradeCosts()
    {
        if (WeaponData != null)
        {
            m_weaponUpgradeCostText.text = WeaponData.UpgradeCost.ToString();
            m_weaponUpgradeCountText.text = $"{GameManager.Instance.m_weaponTiersIndex + 1}/{GameManager.Instance.m_weaponTiers.Length}";
        }

        if (ShieldData != null)
        {
            m_shieldUpgradeCostText.text = ShieldData.UpgradeCost.ToString();
            m_shieldUpgradeCountText.text = $"{GameManager.Instance.m_shieldTiersIndex + 1}/{GameManager.Instance.m_shieldTiers.Length}";
        }

        if (SpeedData != null)
        {
            m_speedUpgradeCostText.text = SpeedData.UpgradeCost.ToString();
            m_speedUpgradeCountText.text = $"{GameManager.Instance.m_speedTiersIndex + 1}/{GameManager.Instance.m_speedTiers.Length}";
        }

        if (ShipData != null)
        {
            m_shipUpgradeCostText.text = ShipData.UpgradeCost.ToString();
            m_shipUpgradeCountText.text = $"{GameManager.Instance.m_shipTiersIndex + 1}/{GameManager.Instance.m_shipTiers.Length}";
        }
    }

    public void RefreshPlanetDefenseSection()
    {
        if (HomePlanet == null || HomePlanet.PlanetData == null)
            return;

        // Buy Buttons
        // 
        if (TurretData != null && (HomePlanet.Gold < TurretData.Cost || HomePlanet.PlanetData.xTurretCount >= TurretData.Max))
            m_turretBuyButton.interactable = false;
        else
            m_turretBuyButton.interactable = true;

        if (HangerData != null && (HomePlanet.Gold < HangerData.Cost || HomePlanet.PlanetData.xHangerCount >= HangerData.Max))
            m_hangerBuyButton.interactable = false;
        else
            m_hangerBuyButton.interactable = true;

        if (PlanetShieldData != null && (HomePlanet.Gold < PlanetShieldData.Cost || HomePlanet.PlanetData.xShieldCount >= PlanetShieldData.Max))
            m_planetShieldBuyButton.interactable = false;
        else
            m_planetShieldBuyButton.interactable = true;

        if (FleetData != null && (HomePlanet.Gold < FleetData.Cost || HomePlanet.PlanetData.xFleetCount >= FleetData.Max))
            m_fleetBuyButton.interactable = false;
        else
            m_fleetBuyButton.interactable = true;

        if (FighterData != null && (HomePlanet.Gold < FighterData.Cost || HomePlanet.PlanetData.xFighterCount >= FighterData.Max))
            m_fighterBuyButton.interactable = false;
        else
            m_fighterBuyButton.interactable = true;

        // Upgrade Buttons
        // 
        if (TurretData != null && (HomePlanet.Gold < TurretData.UpgradeCost || m_gameManager.m_turretTiersIndex + 1 >= m_gameManager.m_turretTiers.Length))
            m_turretUpgradeButton.interactable = false;
        else
            m_turretUpgradeButton.interactable = true;

        if (HangerData != null && (HomePlanet.Gold < HangerData.UpgradeCost || m_gameManager.m_hangerTiersIndex + 1 >= m_gameManager.m_hangerTiers.Length))
            m_hangerUpgradeButton.interactable = false;
        else
            m_hangerUpgradeButton.interactable = true;

        if (FighterData != null && (HomePlanet.Gold < FighterData.UpgradeCost || m_gameManager.m_fighterTiersIndex + 1 >= m_gameManager.m_fighterTiers.Length))
            m_fighterUpgradeButton.interactable = false;
        else
            m_fighterUpgradeButton.interactable = true;

        if (PlanetShieldData != null && (HomePlanet.Gold < PlanetShieldData.UpgradeCost || m_gameManager.m_planetShieldTiersIndex + 1 >= m_gameManager.m_planetShieldTiers.Length))
            m_planetShieldUpgradeButton.interactable = false;
        else
            m_planetShieldUpgradeButton.interactable = true;

        if (FleetData != null && (HomePlanet.Gold < FleetData.UpgradeCost || m_gameManager.m_fleetTiersIndex + 1 >= m_gameManager.m_fleetTiers.Length))
            m_fleetUpgradeButton.interactable = false;
        else
            m_fleetUpgradeButton.interactable = true;

        if (HomePlanet.Gold < HomePlanet.PlanetData.UpgradeCost || m_gameManager.m_planetTiersIndex + 1 >= m_gameManager.m_planetTiers.Length)
            m_planetUpgradeButton.interactable = false;
        else
            m_planetUpgradeButton.interactable = true;
    }

    public void UpgradeWeapon()
    {
        GameManager.Instance.UpgradeWeapon();
        RefreshPlayerShipSection();
    }

    public void UpgradeShield()
    {
        GameManager.Instance.UpgradeShield();
        RefreshPlayerShipSection();
    }

    public void UpgradeSpeed()
    {
        GameManager.Instance.UpgradeSpeed();
        RefreshPlayerShipSection();
    }

    public void UpgradeShip()
    {
        GameManager.Instance.UpgradeShip();
        RefreshPlayerShipSection();
    }

    public void UpgradeWaterTank()
    {
        // check if there is more waterTanks to upgrade to
        if (m_waterTankIndex < m_waterTankUpgrades.Length)
        {
            HomePlanet.RemoveGold(m_waterTankCost);
            if (m_waterTankCost < m_maxUpgradeCost)
                m_waterTankCost += m_waterTankIncrease;

            // Upgrade waterTanks
            m_playerShip.m_waterTankPrefab = m_waterTankUpgrades[m_waterTankIndex];
            m_playerShip.EquipWaterTank();
            m_waterTankIndex++;
        }
    }

    public void BuyTurret()
    {
        if (HomePlanet == null)
            Debug.Log("home planet is null");
        if (TurretData == null)
            Debug.Log("turret data is null");

        HomePlanet.RemoveGold(TurretData.Cost);
        if (HomePlanet.PlanetData.xTurretCount < TurretData.Max)
        {
            HomePlanet.AddTurret(TurretData);
        }

        m_turretCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xTurretCount}/{TurretData.Max}";

        GameManager.Instance.DelayedSave();
        RefreshPlanetDefenseSection();
    }

    public void BuyHanger()
    {
        HomePlanet.RemoveGold(HangerData.Cost);
        if (HomePlanet.PlanetData.xHangerCount < HangerData.Max)
        {
            HomePlanet.AddHanger(HangerData);
        }

        m_hangerCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xHangerCount}/{HangerData.Max}";

        GameManager.Instance.DelayedSave();
        RefreshPlanetDefenseSection();
    }

    public void BuyFighter()
    {
        HomePlanet.RemoveGold(FighterData.Cost);
        if (HomePlanet.PlanetData.xFighterCount <FighterData.Max)
        {
            HomePlanet.AddFighter(FighterData);
        }

        m_fighterCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xFighterCount}/{FighterData.Max}";

        GameManager.Instance.DelayedSave();
        RefreshPlanetDefenseSection();
    }

    public void BuyPlanetShield()
    {
        HomePlanet.RemoveGold(PlanetShieldData.Cost);
        if (HomePlanet.PlanetData.xShieldCount < PlanetShieldData.Max)
        {
            HomePlanet.AddShield(PlanetShieldData);
        }

        m_planetShieldCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xShieldCount}/{PlanetShieldData.Max}";

        GameManager.Instance.DelayedSave();
        RefreshPlanetDefenseSection();
    }

    public void BuyFleet()
    {
        HomePlanet.RemoveGold(FleetData.Cost);
        if (HomePlanet.PlanetData.xFleetCount < FleetData.Max)
        {
            HomePlanet.AddFleet(FleetData);
        }

        m_fleetCountText.text = $"{GameManager.Instance.HomePlanet.PlanetData.xFleetCount}/{FleetData.Max}";

        GameManager.Instance.DelayedSave();
        RefreshPlanetDefenseSection();
    }

    public void UpgradeTurret()
    {
        GameManager.Instance.UpgradeTurret();
        RefreshPlanetDefenseSection();
    }
    
    public void UpgradeHanger()
    {
        GameManager.Instance.UpgradeHanger();
        RefreshPlanetDefenseSection();
    }
    
    public void UpgradeFighter()
    {
        GameManager.Instance.UpgradeFighter();
        RefreshPlanetDefenseSection();
    }
    
    public void UpgradePlanetShield()
    {
        GameManager.Instance.UpgradePlanetShield();
        RefreshPlanetDefenseSection();
    }
    
    public void UpgradeFleet()
    {
        GameManager.Instance.UpgradeFleet();
        RefreshPlanetDefenseSection();
    }
    
    public void UpgradePlanet()
    {
        GameManager.Instance.UpgradePlanet();
        RefreshPlanetDefenseSection();
    }

    public void Attack()
    {
        GameManager.Instance.InvadeEnemyPlanet();
    }
}
