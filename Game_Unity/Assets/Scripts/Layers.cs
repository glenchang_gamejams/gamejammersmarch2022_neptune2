using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Layers
{
    public static readonly int Player = LayerMask.NameToLayer("Player");
    public static readonly int Enemy = LayerMask.NameToLayer("Enemy");
    public static readonly int CanHitPlayer = LayerMask.NameToLayer("CanHitPlayer");
    public static readonly int CanHitEnemy = LayerMask.NameToLayer("CanHitEnemy");
    public static readonly int Planet = LayerMask.NameToLayer("Planet");

}
